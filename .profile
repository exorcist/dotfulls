#! /bin/sh

env="${XDG_CONFIG_HOME:-"$HOME/.config"}/sh/env"
cache="${XDG_CACHE_HOME:-"/tmp/cache"}"

[ -f "$env" ] && . "$env"
[ ! -d "$cache" ] && mkdir -p "$cache"

[ "$(tty)" = "/dev/tty1" ] && [ ! "$(pidof Xorg)" ] && \
    startx "${XINITRC:-"$XDG_CONFIG_HOME/x/init"}" -- "vt$XDG_VTNR" >"/dev/null" 2>&1 && exit 0 || return 0

# vim:ft=sh
