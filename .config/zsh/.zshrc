#! /usr/bin/env zsh

aliasrc="${XDG_CONFIG_HOME:-"$HOME/.config"}/sh/aliasrc"
functionrc="${XDG_CONFIG_HOME:-"$HOME/.config"}/sh/functionrc"

[ -f "$aliasrc" ] && source "$aliasrc"
[ -f "$functionrc" ] && source "$functionrc"

HISTSIZE="1000000"
SAVEHIST="1000000"

zstyle :compinstall filename "${XDG_CONFIG_HOME:-"$HOME/.config"}/zsh/.zshrc"
zmodload zsh/complist

autoload -Uz colors
colors

autoload -Uz promptinit
promptinit

autoload -U compinit
compinit -u -d "${XDG_CACHE_HOME:-"/tmp/cache"}/zcompdump-${ZSH_VERSION:-"5.8"}"

zstyle ":completion:*" sort false
zstyle ":completion:*" list-colors "${LS_COLORS}"
zstyle ":completion:*" menu select
zstyle ":completion:*" special-dirs true
zstyle ":completion:*" ignored-patterns
zstyle ":completion:*" completer _complete
zstyle ":completion:*:*:kill:*:processes" list-colors "=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01"
zstyle ":completion:*:*:*:*:processes" command "ps -u "$USER" -o pid,user,cmd -w -w"
zstyle ":completion:*" matcher-list "m:{a-zA-Z}={A-Za-z}" "r:|=*" "l:|=* r:|=*"
_comp_options+=(globdots)

autoload -U edit-command-line
zle -N edit-command-line

zle -N zle-line-init
zle -N zle-line-finish
zle -N zle-keymap-select
zle -N zle-line-init

bindkey "^e" edit-command-line
bindkey -s "^w" "fcd\n"
bindkey -s "^x" "cdup\n"
bindkey -s "^a" "back\n"
bindkey -s "^s" "asciinema rec\n"

export KEYTIMEOUT="1"
bindkey -v
bindkey "^?" backward-delete-char

set -k

while read -r i
do
    setopt "$i"
done <<-EOF
autocd
nobeep
promptsubst
alwaystoend
appendhistory
automenu
completeinword
extendedhistory
histexpiredupsfirst
histignoredups
histignorespace
histverify
inc_append_history
interactivecomments
sunkeyboardhack
EOF

# setopt correct

while read -r i
do
    unsetopt "$i"
done <<-EOF
flowcontrol
nomatch
correct
equals
EOF

zle-line-init () {
    echoti smkx
}

zle-line-finish () {
    echoti rmkx
}

zle-keymap-select () {
  if
      [[ "$KEYMAP" == "vicmd" || "$1" = "block" ]]
  then
    print -n "\e[2 q"
  elif
      [[ "$KEYMAP" == "main" || "$KEYMAP" == "viins" || -z "$KEYMAP" || "$1" = "beam" ]]
  then
    print -n "\e[6 q"
  fi
}

zle-line-init () {
  zle -K viins
  print -n "\e[6 q"
}
print -n "\e[6 q"

preexec () {
    print -n "\e[6 q"
}

case "$-" in
    *i*)
        ;;
    *)
        return
        ;;
esac

PS1="%(?.%(!.%{$fg[blue]%}%{$bg[black]%}%B%1~%b%{$fg[red]%}%{$bg[black]%} #.%{$fg[blue]%}%{$bg[black]%}%B%1~%b%{$fg[green]%}%{$bg[black]%} λ).%{$fg[blue]%}%{$bg[black]%}%B%1~%b%{$fg[red]%}%{$bg[black]%} λ)%{$reset_color%} %f"
PS2="%{$fg[magenta]%}%{$bg[black]%}%_ %{$reset_color%}%{$fg[green]%}%{$bg[black]%}λ%{$reset_color%} %f"

# vim:ft=zsh
