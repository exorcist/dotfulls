{-# LANGUAGE LambdaCase #-}

{- |
   Module : Config.Prompts
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Config.Prompts ( promptTheme
                      , noHistTheme
                      , warningTheme
                      , unicodeTheme
                      , criticalTheme
                      , torrentPrompt
                      , appImagePrompt
                      , uploadFilePrompt
                      , termLaunchPrompt
                      ) where

import Prelude ( ($)
               , (.)
               , (<>)
               , (>>=)
               , (<$>)
               , IO ()
               , Bool ( True
                      , False
                      )
               , String ()
               )


import Data.Char (toLower)
import Data.List ( concat
                 , isInfixOf
                 )
import Data.Maybe ( Maybe ( Just
                          , Nothing
                          )
                  )

import System.IO.Temp (emptyTempFile)
import System.FilePath ( (</>)
                       , takeExtension
                       )
import System.Directory ( Permissions (executable)
                        , copyFile
                        , listDirectory
                        , doesFileExist
                        , getPermissions
                        , setPermissions
                        , doesDirectoryExist
                        , getCurrentDirectory
                        , setCurrentDirectory
                        )

import Control.Monad (when)
import Control.Monad.IO.Class (liftIO)

import Graphics.X11.Types ( xK_equal
                          , shiftMask
                          )

import XMonad ( X ()
              , spawn
              )

import XMonad.Prompt ( XPConfig ( font
                                , height
                                , sorter
                                , bgColor
                                , fgColor
                                , bgHLight
                                , fgHLight
                                , position
                                , historySize
                                , borderColor
                                , maxComplRows
                                , autoComplete
                                , promptKeymap
                                , completionKey
                                , historyFilter
                                , defaultPrompter
                                , maxComplColumns
                                , searchPredicate
                                , alwaysHighlight
                                , promptBorderWidth
                                , prevCompletionKey
                                , showCompletionOnTab
                                , complCaseSensitivity
                                )
                     , XPPosition (Top)
                     , ComplCaseSensitivity (CaseInSensitive)
                     , def
                     , defaultXPKeymap
                     , mkComplFunFromList
                     , deleteAllDuplicates
                     )
import XMonad.Prompt.Shell (getCommands)
import XMonad.Prompt.Input ( (?+)
                           , inputPromptWithCompl
                           )
import XMonad.Prompt.FuzzyMatch ( fuzzySort
                                , fuzzyMatch
                                )

import App.Alias ( term
                 , homeDir
                 , dataHome
                 , dataDrive
                 , spawnList
                 )

import Debug.Debug (notify)

import Theme.Theme ( basebg
                   , basefg
                   , base01
                   , base03
                   , base04
                   , base09
                   , myFont
                   , myBoldFont
                   )

promptTheme :: XPConfig
promptTheme = def { height = 30
                  , font = myFont
                  , position = Top
                  , bgColor = basebg
                  , fgColor = basefg
                  , bgHLight = base09
                  , fgHLight = basebg
                  , sorter = fuzzySort
                  , historySize = 8192
                  , borderColor = base04
                  , promptBorderWidth = 2
                  , maxComplRows = Just 10
                  , autoComplete = Nothing
                  , defaultPrompter = (toLower <$>)
                  , alwaysHighlight = True
                  , maxComplColumns = Just 10
                  , showCompletionOnTab = False
                  , completionKey = (0, xK_equal)
                  , searchPredicate = fuzzyMatch
                  , promptKeymap = defaultXPKeymap
                  , historyFilter = deleteAllDuplicates
                  , complCaseSensitivity = CaseInSensitive
                  , prevCompletionKey = (shiftMask, xK_equal)
                  }

noHistTheme :: XPConfig
noHistTheme = promptTheme { historySize = 0 }

unicodeTheme :: XPConfig
unicodeTheme = promptTheme { font = concat [ myFont
                                           , ","
                                           , "Noto Color Emoji:style=Regular:pixelsize=14:antialias=true:hinting=true"
                                           , ","
                                           , "Apple Color Emoji:style=Regular:pixelsize=14:antialias=true:hinting=true"
                                           ]
                           , searchPredicate = isInfixOf
                           }

warningTheme :: XPConfig
warningTheme = promptTheme { bgColor = base03
                           , fgColor = basebg
                           , font = myBoldFont
                           , promptBorderWidth = 0
                           , promptKeymap = defaultXPKeymap
                           }

criticalTheme :: XPConfig
criticalTheme = promptTheme { bgColor = base01
                            , fgColor = basebg
                            , font = myBoldFont
                            , promptBorderWidth = 0
                            , promptKeymap = defaultXPKeymap
                            }

appImagePrompt :: XPConfig -> X ()
appImagePrompt c = inputPromptWithCompl c "run" (appImageCompl c) ?+ \input ->
    spawn $ dataHome </> "appimages" </> input

appImageCompl :: XPConfig -> String -> IO [String]
appImageCompl c s = do contents <- listDirectory $ dataHome </> "appimages"
                       mkComplFunFromList c contents s

torrentPrompt :: XPConfig -> X ()
torrentPrompt c = inputPromptWithCompl c "pick a torrent directory" (torrentDirCompl c) ?+ \case
      x -> inputPromptWithCompl c "pick a torrent" (torrentCompl c x) ?+ \inp ->
          inputPromptWithCompl c "pick a save directory" (saveDirectoryCompl c) ?+ \case
          y -> spawnList ["btcli", "add", "-n", ['"'] <> inp <> ['"'], "-d", y, x </> ("\"" <> inp <> "\"")]

dl, isoDir, movDir, torDir :: String
dl = homeDir </> "dl"
isoDir = dataDrive </> "ISOS"
movDir = dataDrive </> "Movies"
torDir = dataDrive </> "Torrents"

saveDirectoryCompl :: XPConfig -> String -> IO [String]
saveDirectoryCompl c = mkComplFunFromList c [isoDir, movDir]

torrentDirCompl :: XPConfig -> String -> IO [String]
torrentDirCompl c = mkComplFunFromList c [dl, torDir]

torrentCompl :: XPConfig -> String -> String -> IO [String]
torrentCompl c s x = listDirectory s >>= \files -> mkComplFunFromList c files x

uploadFilePrompt :: XPConfig -> X ()
uploadFilePrompt c = inputPromptWithCompl c "pick an entry" (uploadCompl c) ?+ \input -> do
    origDir <- liftIO getCurrentDirectory
    dir <- liftIO $ doesDirectoryExist input
    file <- liftIO $ doesFileExist input

    when dir $ do
           _ <- liftIO $ setCurrentDirectory input
           uploadFilePrompt c
    when file . liftIO $ emptyTempFile "/tmp" ("upload" <> takeExtension input) >>= \temp -> do
               liftIO $ copyFile input temp

               perms <- liftIO $ getPermissions temp

               liftIO $ setPermissions temp (perms { executable = False })

               spawnList [ "curl", "-fsLF", "'file=@" <> temp <> "'", "https://0x0.st"
                         , "|", "tr", "-d", "'\n'"
                         , "|", "xsel", "-b", "-i"
                         ]
               notify "0x0" "link copied to clipboard!"

    liftIO $ setCurrentDirectory origDir

uploadCompl :: XPConfig -> String -> IO [String]
uploadCompl c s = getCurrentDirectory >>= listDirectory >>= \files -> mkComplFunFromList c files s

termLaunchPrompt :: XPConfig -> X ()
termLaunchPrompt c = inputPromptWithCompl c "launch" (termCompl c) ?+ \input ->
    spawnList [term, "--role=" <> "tym-float", "-e", input]

termCompl :: XPConfig -> String -> IO [String]
termCompl c s = getCommands >>= \cmd -> mkComplFunFromList c cmd s

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
