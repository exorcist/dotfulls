{- |
   Module : Config.GridSelect
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Config.GridSelect ( gsConfig
                         , defGSConfig
                         ) where


import Prelude ( (<>)
               , Bool ()
               , String ()
               , Integer ()
               , pure
               , otherwise
               )

import XMonad (X ())
import XMonad.Actions.GridSelect ( GSConfig ( gs_font
                                            , gs_navigate
                                            , gs_cellwidth
                                            , gs_cellheight
                                            , gs_bordercolor
                                            , gs_cellpadding
                                            )
                                 , navNSearch
                                 , buildDefaultGSConfig
                                 )

import Theme.Theme ( basebg
                   , basefg
                   , base01
                   , base05
                   , myFont
                   )

defGSConfig :: GSConfig a
defGSConfig = gsConfig 100

gsConfig :: Integer -> GSConfig a
gsConfig width = (buildDefaultGSConfig colorizer) { gs_cellheight = 50
                                                  , gs_cellpadding = 30
                                                  , gs_cellwidth = width
                                                  , gs_bordercolor = base01
                                                  , gs_navigate = navNSearch
                                                  , gs_font = myFont <> "," <> "Noto Color Emoji:style=Regular:pixelsize=14:antialias=true:hinting=true"
                                                  }

colorizer :: a -> Bool -> X (String, String)
colorizer _ p | p = pure (base05, basebg)
              | otherwise = pure (basebg, basefg)

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
