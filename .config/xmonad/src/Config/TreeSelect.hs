{- |
   Module : Config.TreeSelect
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Config.TreeSelect ( treeConfig
                         , treeActions
                         ) where

import Prelude ( ($)
               , (.)
               , (<>)
               , (*>)
               , Bool (True)
               , Monad ()
               , String ()
               , read
               , null
               , pure
               , otherwise
               )

import Data.Tree (Tree (Node))

import Control.Monad.IO.Class (liftIO)

import System.Exit (exitSuccess)
import System.FilePath ((</>))

import XMonad ( X ()
              , LayoutMessages (ReleaseResources)
              , spawn
              , unGrab
              , restart
              , broadcastMessage
              )

import XMonad.Actions.TreeSelect ( Pixel ()
                                 , TSNode (TSNode)
                                 , TSConfig ( TSConfig
                                            , ts_font
                                            , ts_node
                                            , ts_extra
                                            , ts_indent
                                            , ts_nodealt
                                            , ts_originX
                                            , ts_originY
                                            , ts_navigate
                                            , ts_highlight
                                            , ts_background
                                            , ts_node_width
                                            , ts_node_height
                                            , ts_hidechildren
                                            )
                                 , defaultNavigation
                                 )

import App.Alias ( (+++)
                 , term
                 , htop
                 , files
                 , music
                 , inTerm
                 , script
                 , browser
                 , homeDir
                 , dataHome
                 , spawnList
                 , termFloat
                 , andExecute
                 , browserAlt
                 , removeHash
                 , inImageViewer
                 )

import Bind.Help (showHelp)

import Config.Prompts ( promptTheme
                      , termLaunchPrompt
                      )

import Theme.Theme ( basebg
                   , basefg
                   , base05
                   , myFont
                   )

readToPixel :: String -> Pixel
readToPixel x | null x = 0xff000000
              | otherwise = read $ "0xff" <> removeHash x

treeConfig :: TSConfig a
treeConfig = TSConfig { ts_font = myFont
                      , ts_node = (readToPixel basefg, readToPixel basebg)
                      , ts_extra = readToPixel basefg
                      , ts_indent = 80
                      , ts_originX = 15
                      , ts_originY = 15
                      , ts_nodealt = (readToPixel basefg, readToPixel basebg)
                      , ts_highlight = (readToPixel basebg, readToPixel base05)
                      , ts_background = readToPixel basebg
                      , ts_node_width = 200
                      , ts_node_height = 32
                      , ts_hidechildren = True
                      , ts_navigate = defaultNavigation
                      }

node :: Monad m => String -> String -> [Tree (TSNode (m ()))] -> Tree (TSNode (m ()))
node a b = Node $ TSNode (a +++ "+") b (pure ())

subNode :: String -> String -> a -> Tree (TSNode a)
subNode a b c = Node (TSNode a b c) []

treeActions :: [Tree (TSNode (X ()))]
treeActions =
    [ node "Session" "Session management"
              [ subNode "Logout" "Exit current XMonad session" (broadcastMessage ReleaseResources *> liftIO exitSuccess)
              , subNode "Lock" "Lock the screen" (spawnList ["xset", "s", "activate"])
              , subNode "Recompile" "Restart the current XMonad session" (broadcastMessage ReleaseResources *> restart "xmonad" True)
              , subNode "Restart" "Restart the computer" (spawnList ["systemctl", "reboot"])
              , subNode "Power Off" "Power off the computer" (spawnList ["systemctl", "poweroff"])
              ]
    , node "Terminal programs" "Launch a program in the terminal"
              [ subNode "st" "Simple Terminal" (spawn term)
              , subNode "fff" "File Manager" (spawn . inTerm . andExecute $ files +++ homeDir)
              , subNode "nsxiv" "Open the image viewer in the wallpapers directory" (spawn . inImageViewer . ("-rt" +++) $ dataHome </> "src" </> "wallpapers" </> "gruvbox")
              , subNode "htop" "System Monitor" (spawn . inTerm . andExecute $ htop)
              , subNode "ncmpcpp" "Music Player" (spawn . inTerm . andExecute $ music)
              , subNode "pulsemixer" "Controls for the pulseaudio sound system" (spawn . inTerm . andExecute $ "pulsemixer")
              ]
    , node "Other programs" "Launch programs that have a graphical user interface"
              [ subNode "firefox" "The firefox web browser" (spawn browser)
              , subNode "discord" "The only proprietary software that I use" (spawn $ script "other/discocss")
              , subNode "qutebrowser" "The qutebrowser ... browser" (spawn browserAlt)
              , subNode "virt-manager" "A manager for the kernel-based virtual machine - KVM and the quick emulator - QEMU" (spawn "virt-manager")
              ]
    , Node (TSNode "Help" "Show all keybinds" showHelp) []
    , node "Themes" "Switch between light and dark mode"
              [ subNode "Light Mode" "Switch to gruvbox light" (spawnList [script "switch", "light"])
              , subNode "Dark Mode" "Switch to gruvbox dark" (spawnList [script "switch", "dark"])
              ]
    , node "Screenshots" "Take screenshots"
              [ subNode "Full" "Take a fullscreen screenshot" (unGrab *> spawnList [script "scr", "full"])
              , subNode "Selection" "Take a screenshot of a selected region" (unGrab *> spawnList [script "scr", "sel"])
              ]
    , node "Terminals" "Open some kind of terminal"
              [ subNode "Tiled" "Open a normal terminal" (spawn term)
              , subNode "Floating" "Open a floating terminal" (unGrab *> termFloat)
              , subNode "Floating with an app" "Open a floating terminal with an application running inside it" (unGrab *> termLaunchPrompt promptTheme)
              ]
    ]

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
