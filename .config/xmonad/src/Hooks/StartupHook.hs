{- |
   Module : Hooks.StartupHook
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Hooks.StartupHook (starter) where

import Prelude (mconcat)

import XMonad ( X ()
              , def
              , startupHook
              )

import XMonad.Hooks.SetWMName (setWMName)

import XMonad.Util.SpawnOnce (spawnOnce)

import App.Alias (soulseek)

starter :: X ()
starter = mconcat [ startupHook def
                  , setWMName "LG3D"
                  , spawnOnce soulseek
                  ]

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
