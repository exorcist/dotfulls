{- |
   Module : Hooks.ManageHook
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Hooks.ManageHook (windowManager) where

import Prelude ( (.)
               , (/)
               , (<$>)
               , (>>=)
               , Bool (False)
               , String ()
               , Rational ()
               , mconcat
               )

import Data.List (isPrefixOf)

import Control.Monad (liftM2)

import XMonad ( (=?)
              , (-->)
              , (<&&>)
              , Query ()
              , ManageHook ()
              , ask
              , def
              , doF
              , title
              , doShift
              , doFloat
              , doIgnore
              , className
              , manageHook
              , stringProperty
              )
import XMonad.StackSet ( RationalRect (RationalRect)
                       , sink
                       , view
                       , shift
                       )

import XMonad.Actions.Sift (siftUp)
import XMonad.Actions.SpawnOn (manageSpawn)
import XMonad.Actions.CopyWindow (copyToAll)

import XMonad.Hooks.ManageHelpers ( isDialog
                                  , doFloatDep
                                  , isInProperty
                                  , doCenterFloat
                                  )

import XMonad.Layout.NoBorders (hasBorder)

import XMonad.Util.NamedScratchpad (namedScratchpadManageHook)

import App.Projects (at)
import App.Scratchpad (scratchpads)

windowManager :: ManageHook
windowManager = mconcat [ manageSpawn
                        , manageHook def
                        , isRole =? "pop-up" --> doFloat
                        , className =? "xmobar" --> doIgnore
                        , isRole =? "About" --> doCenterFloat
                        , namedScratchpadManageHook scratchpads
                        , className =? "Nsxiv" --> hasBorder False
                        , className =? "Picker" --> doCenterFloat
                        , isName =? "Library" --> forceCenterFloat
                        , className =? "Places" --> forceCenterFloat
                        , className =? "New VM" --> forceCenterFloat
                        , className =? "Signal" --> shiftFocus (at 2)
                        , className =? "ViberPC" --> shiftFocus (at 2)
                        , className =? "Ripcord" --> shiftFocus (at 2)
                        , className =? "discord" --> shiftFocus (at 2)
                        , className =? "CloudCompare" --> doShift (at 5)
                        , className =? "FileZilla" --> shiftFocus (at 6)
                        , className =? "SoulseekQt" --> shiftFocus (at 7)
                        , className =? "easyeffects" --> shiftFocus (at 8)
                        , className =? "qutebrowser" --> shiftFocus (at 1)
                        , isDialog --> mconcat [doF siftUp, doCenterFloat]
                        , className =? "TelegramDesktop" --> shiftFocus (at 2)
                        , className =? "mpv" --> mconcat [doF siftUp, unFloat]
                        , isRole =? "GtkFileChooserDialog" --> forceCenterFloat
                        , className =? "Minecraft Launcher" --> shiftFocus (at 10)
                        , ("Minecraft*" `isPrefixOf`) <$> className --> shiftFocus (at 10)
                        , className =? "Ripcord" <&&> isName =? "Preferences" --> forceCenterFloat
                        , title =? "Picture-in-Picture" --> mconcat [forceCenterFloat, doF copyToAll]
                        , className =? "help" --> mconcat [forceCenterFloat, doF siftUp, hasBorder False]
                        , isInProperty "_NET_WM_WINDOW_TYPE" "_NET_WM_WINDOW_TYPE_SPLASH" --> doCenterFloat
                        ]

shiftFocus :: String -> ManageHook
shiftFocus = doF . liftM2 (.) view shift

unFloat :: ManageHook
unFloat = ask >>= doF . sink

isName :: Query String
isName = stringProperty "WM_NAME"

isRole :: Query String
isRole = stringProperty "WM_WINDOW_ROLE"

forceCenterFloat :: ManageHook
forceCenterFloat = doFloatDep center where
    center :: RationalRect -> RationalRect
    center _ = RationalRect w h x y

    w, h, x, y :: Rational
    w = 1/6
    h = 1/6
    x = 2/3
    y = 2/3

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
