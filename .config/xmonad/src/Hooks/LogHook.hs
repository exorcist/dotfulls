{- |
   Module : Hooks.LogHook
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Hooks.LogHook ( logger
                     , statusBar
                     ) where

import Prelude ( ($)
               , (<>)
               , (<$>)
               , (=<<)
               , String ()
               , const
               , concat
               , mconcat
               )

import Data.Maybe (fromMaybe)
import Data.Char (toLower)

import XMonad ( X ()
              , def
              , logHook
              )
import XMonad.Actions.CopyWindow (copiesPP)

import XMonad.Hooks.Modal (logMode)
import XMonad.Hooks.StatusBar ( StatusBarConfig ()
                              , statusBarProp
                              , xmonadPropLog'
                              )
import XMonad.Hooks.DynamicLog ( PP ( ppSort
                                    , ppTitle
                                    , ppLayout
                                    , ppUrgent
                                    , ppHidden
                                    , ppExtras
                                    , ppCurrent
                                    , ppHiddenNoWindows
                                    )
                               , wrap
                               , filterOutWsPP
                               )

import XMonad.Util.NamedScratchpad (scratchpadWorkspaceTag)
import XMonad.Util.WorkspaceCompare (getSortByIndex)
import XMonad.Util.ClickableWorkspaces (clickablePP)

import Theme.Theme ( basebg
                   , base01
                   , base03
                   , base04
                   , base05
                   , base12
                   )

logger :: X ()
logger = mconcat [ logCurMode
                 , logHook def
                 ]

logCurMode :: X ()
logCurMode = do
    m <- logMode

    let mode :: String
        mode = case fromMaybe [] m of
                 [] -> []
                 x -> concat [ "<fn=4><fc="
                             , basebg, ",", base12
                             , ":5> \xf065 </fc></fn><fn=0><fc="
                             , basebg, ",", base12
                             , ":5>"
                             , toLower <$> x
                             , " </fc></fn>"
                             ]

    xmonadPropLog' "_XMONAD_MODE" mode

statusBar :: StatusBarConfig
statusBar = do
    let hidden, urgent, focused :: String -> String
        hidden = wrap ("<fc=" <> basebg <> "," <> base01 <> ":5> ") " </fc>"
        urgent = wrap ("<fn=2><fc=" <> basebg <> "," <> base03 <> ":5> ") " </fc></fn>"
        focused = wrap ("<fc=" <> basebg <> "," <> base05 <> ":5> ") " </fc>"

        barPP :: PP
        barPP = def
            { ppExtras = []
            , ppUrgent = urgent
            , ppHidden = hidden
            , ppTitle = const ""
            , ppLayout = const ""
            , ppCurrent = focused
            , ppSort = getSortByIndex
            , ppHiddenNoWindows = const ""
            }

    statusBarProp "xmobar" $ clickablePP
      =<< copiesPP (wrap ("<fc=" <> basebg <> "," <> base04 <> ":5> ") " </fc>")
                   (filterOutWsPP [scratchpadWorkspaceTag] barPP)

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
