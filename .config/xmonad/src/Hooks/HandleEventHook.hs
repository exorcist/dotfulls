{- |
   Module : Hooks.HandleEventHook
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use head" #-}

module Hooks.HandleEventHook (handler) where

import Prelude ( ($)
               , (.)
               , (-)
               , (+)
               , (/)
               , (/=)
               , (!!)
               , (<$>)
               , Bool (True)
               , String ()
               , Rational ()
               , pure
               , read
               , words
               , filter
               , return
               , mconcat
               , toRational
               )

import Data.Monoid (All (All))

import Control.Monad (when)

import XMonad ( (=?)
              , X ()
              , Event ( MapRequestEvent
                      , ev_window
                      )
              , Query ()
              , def
              , windows
              , runQuery
              , className
              , stringProperty
              , handleEventHook
              )

import XMonad.StackSet ( RationalRect (RationalRect)
                       , float
                       )

import XMonad.Actions.UpdateFocus (focusOnMouseMove)

import XMonad.Hooks.FadeWindows (fadeWindowsEventHook)
import XMonad.Hooks.WindowSwallowing (swallowEventHook)

import XMonad.Layout.LayoutHints (hintsEventHook)

import XMonad.Util.Run (runProcessWithInput)

import Theme.Theme (base11)

handler :: Event -> X All
handler = mconcat [ mconcat $ swallower <$> ["tym"]
                  , hintsEventHook
                  , focusOnMouseMove
                  , handleEventHook def
                  , fadeWindowsEventHook
                  , positioner (isRole =? "tym-float")
                  ]

isRole :: Query String
isRole = stringProperty "WM_WINDOW_ROLE"

swallower :: String -> Event -> X All
swallower prog = swallowEventHook (className =? prog) (pure True)

hacksaw :: X [Rational]
hacksaw = do
   str <- runProcessWithInput "hacksaw" [ "-f", "%x %y %w %h"
                                        , "-g", "2"
                                        , "-s", "2"
                                        , "-r", "1"
                                        , "-c", base11
                                        ] []

   return $ toRational . read <$> filter (/= "\n") (words str)

positioner :: Query Bool -> Event -> X All
positioner qry event = do
   case event of
     MapRequestEvent { ev_window = win } -> do
        g <- runQuery qry win

        when g $ do
           list <- hacksaw

           let geometry :: [Rational]
               geometry = (/ 1500) <$> [(list !! 0)-150, (list !! 1)+150, (list !! 2)-150, (list !! 3)+150]

           windows $ float win (RationalRect (geometry !! 0) (geometry !! 1) (geometry !! 2) (geometry !! 3))
     _ -> return ()

   return $ All True

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
