{- |
   Module : Main (for xmobar)
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Main (main) where

import Prelude ( ($)
               , (.)
               , (>)
               , (&&)
               , (==)
               , (<=)
               , (<>)
               , (!!)
               , (*>)
               , (>>=)
               , IO ()
               , Int ()
               , Bool ( True
                      , False
                      )
               , Read ()
               , Show ()
               , Maybe (Nothing)
               , String ()
               , show
               , null
               , read
               , pure
               , concat
               , otherwise
               )

import System.Process ( waitForProcess
                      , runInteractiveProcess
                      )
import System.IO ( hGetLine
                 , hSetBinaryMode
                 )

import Control.Monad (void)

import Xmobar

import App.Alias ( (+++)
                 , term
                 , script
                 )

import Theme.Theme ( basebg
                   , basefg
                   , base00
                   , base08
                   , base01
                   , base09
                   , base02
                   , base10
                   , base03
                   , base11
                   , base04
                   , base12
                   , base05
                   , base13
                   , base06
                   , base14
                   , base07
                   , base15
                   , base16
                   , base17
                   , base18
                   , xmobarFont
                   , fontAwesome
                   , xmobarNerdFont
                   , xmobarBoldFont
                   )

data Record = Record deriving (Read, Show)
instance Exec Record where
    alias _ = "rec"
    run _ = pure . inFGBG 0 12 . inFont 4 $ "<action=`" <> script "other/record`> \xf03d </action>"

data Bluetooth = Bluetooth String Int deriving (Read, Show)
instance Exec Bluetooth where
    alias _ = "bluetooth"
    start (Bluetooth off r) s = go where

        go :: IO a
        go = runCmd (s . replace) (script "btinfo") [] *> tenthSeconds r *> go

        ch :: String
        ch = "charge"

        replace :: String -> String
        replace c | null c = inFG 8 off
                  | c == off = inFG 8 c
                  | read c <= 100 && read c > 80 = inFG 2 ch <> ":" +++ c <> "%"
                  | read c <= 80 && read c > 60 = inFG 10 ch <> ":" +++ c <> "%"
                  | read c <= 60 && read c > 40 = inFG 3 ch <> ":" +++ c <> "%"
                  | read c <= 40 && read c > 20 = inFG 11 ch <> ":" +++ c <> "%"
                  | read c <= 20 && read c > 0 = inFG 1 ch <> ":" +++ c <> "%"
                  | otherwise = inFG 8 c

inFont :: Int -> String -> String
inFont i t = "<fn=" <> show i <> ">" <> t <> "</fn>"

inFG :: Int -> String -> String
inFG c t = "<fc=" <> base c <> "," <> base 19 <> ":0>" <> t <> "</fc>"

-- inBG :: Int -> String -> String
-- inBG c t = "<fc=" <> base 19 <> "," <> base c <> ":0>" <> t <> "</fc>"

inFGBG :: Int -> Int -> String -> String
inFGBG f b t = "<fc=" <> base f <> "," <> base b <> ":5>" <> t <> "</fc>"

runCmd :: (String -> IO ()) -> String -> [String] -> IO ()
runCmd b c a = do (_, hstdout, _, p) <- runInteractiveProcess c a Nothing Nothing
                  hSetBinaryMode hstdout False
                  hGetLine hstdout >>= b
                  void $ waitForProcess p

base :: Int -> String
base = ([ base00
        , base01
        , base02
        , base03
        , base04
        , base05
        , base06
        , base07
        , base08
        , base09
        , base10
        , base11
        , base12
        , base13
        , base14
        , base15
        , base16
        , base17
        , base18
        , basebg
        , basefg
        ] !!)

volActions :: String -> String
volActions k = concat [ "<action=`" <> script "vol" +++ "mute` button=1>"
                      , "<action=`pgrep -x pulsemixer ||" +++ term +++ "-e pulsemixer` button=3>"
                      , "<action=`" <> script "vol" +++ "+` button=4>"
                      , "<action=`" <> script "vol" +++ "-` button=5>"
                      , k
                      , "</action>"
                      , "</action>"
                      , "</action>"
                      , "</action>"
                      ]

runCommands :: [Runnable]
runCommands = [ Run UnsafeXMonadLog
              , Run $ UnsafeNamedXPropertyLog "_XMONAD_MODE" "mode"
              , Run $ Bluetooth "bt" 600
              , Run $ MPD [ "-t"
                          , "<statei>"
                          , "--"
                          , "-P"
                          , inFGBG 0 2 (inFont 4 " \xf8ab ") <> inFGBG 0 2 (inFont 0 "<track> ")
                                                    +++ inFGBG 0 16 (inFont 0 " <title> ")
                          , "-Z"
                          , inFGBG 0 3 $ inFont 4 " \xf04c "
                          , "-S"
                          , inFGBG 0 1 $ inFont 4 " \xf05e "
                          ] 300
              , Run Record
              , Run $ Alsa "default" "Master" [ "-t"
                                              , "<status>"
                                              , "--"
                                              , "-C", base 20
                                              , "-c", base 20
                                              , "-O", inFGBG 0 6 " <volume>% "
                                              , "-o", inFGBG 0 13 (inFont 4 " \xf2e2 ")
                                              , "-H", "55"
                                              , "-L", "10"
                                              , "-h", inFGBG 0 9 (inFont 4 " \xf028 ")
                                              , "-m", inFGBG 0 10 (inFont 4 " \xf6a8 ")
                                              , "-l", inFGBG 0 13 (inFont 4 " \xf027 ")
                                              ]
              , Run $ DynNetwork ["-t"
                                 , "" +++ inFGBG 0 11 (inFont 4 " \xf796 ")
                                   <> inFGBG 0 16 (inFont 4 " \xf063 ")
                                   <> inFGBG 0 16 "<rx>KB "
                                   <> inFGBG 0 6 (inFont 4 " \xf062 ")
                                   <> inFGBG 0 6 "<tx>KB "
                                   +++ ""
                                 , "--"
                                 , "--devices", "enp7s0f1,wlan0"
                                 ] 30
              , Run $ BatteryP ["BAT1"]
                               [ "-t"
                               , "<acstatus>"
                               , "-a"
                               , "notify-send -t 10000 -u critical 'Battery is running out!'"
                               , "--"
                               , "-P"
                               , "-L"
                               , "-15"
                               , "-H"
                               , "-5"
                               , "-l"
                               , base 20
                               , "-m"
                               , base 20
                               , "-h"
                               , base 20
                               , "-p"
                               , base 20
                               , "-O"
                               , inFGBG 0 10 (inFont 4 " \xf376 ") <> inFGBG 0 10 "<left> "
                               , "-o"
                               , inFGBG 0 1 (inFont 4 " \xf244 ") <> inFGBG 0 1 "<left> (<timeleft>h) "
                               , "-i"
                               , inFGBG 0 2 (inFont 4 " \xf1e6 ") <> inFGBG 0 2 "<left> "
                               , "-A"
                               , "3"
                               ] 600
              , Run $ Date (inFGBG 0 5 (inFont 4 " \xf133 ") <> inFGBG 0 5 (inFont 0 "%d.%m.%y ")) "date" 36000
              , Run $ Date (inFGBG 0 3 (inFont 4 " \xf017 ") <> inFGBG 0 3 (inFont 0 "%H:%M ")) "time" 600
              ]

config :: Config
config = defaultConfig { font = additionalFonts config !! 2
                       , additionalFonts = [ xmobarNerdFont
                                           , xmobarBoldFont
                                           , xmobarFont
                                           , fontAwesome
                                           ]
                       , dpi = 96
                       , textOffset = 1
                       , textOffsets = [1, textOffset config, textOffset config, 0]
                       , borderColor = base 20
                       , borderWidth = 0
                       , border = NoBorder
                       , bgColor = base 19
                       , fgColor = base 20
                       , alpha = 255
                       , position = TopH 30
                       , iconOffset = 0
                       , lowerOnStart = True
                       , pickBroadest = False
                       , persistent = True
                       , hideOnStart = False
                       , allDesktops = True
                       , overrideRedirect = True
                       , commands = runCommands
                       , sepChar = "%"
                       , alignSep = "}{"
                       , template = "%mode%" +++
                                    "%mpd%" +++
                                    "}" +++ "%UnsafeXMonadLog%" +++ "{" +++
                                    "%rec%" +++
                                    volActions "%alsa:default:Master%" <>
                                    "%dynnetwork%" <>
                                    "%battery%" +++
                                    "%date%" +++
                                    "%time%" +++ ""
                       }


main :: IO ()
main = xmobar config
