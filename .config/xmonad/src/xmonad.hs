{- |
   Module : Main
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
   First Edit: 13.11.2020 (dd.mm.yyyy), Best date format
-}

module Main (main) where

import Prelude ( ($)
               , (.)
               , IO ()
               )

import XMonad ( XConfig ( keys
                        , modMask
                        , logHook
                        , terminal
                        , manageHook
                        , layoutHook
                        , workspaces
                        , borderWidth
                        , startupHook
                        , mouseBindings
                        , handleEventHook
                        , clickJustFocuses
                        , focusFollowsMouse
                        , normalBorderColor
                        , focusedBorderColor
                        )
              , def
              , xmonad
              , mod4Mask
              )

import XMonad.Actions.DynamicProjects (dynamicProjects)

import XMonad.Hooks.Modal (modal)
import XMonad.Hooks.StatusBar (withSB)
import XMonad.Hooks.ManageDocks (docks)
import XMonad.Hooks.UrgencyHook (withUrgencyHook)
import XMonad.Hooks.EwmhDesktops ( ewmh
                                 , ewmhFullscreen
                                 )

import App.Alias ( term
                 , clickFocus
                 , focusMouse
                 , normalColor
                 , focusedColor
                 )
import App.Projects ( spaces
                    , projects
                    )

import Bind.KeyBoard ( modes
                     , mappings
                     )
import Bind.Mouse (mouse)

import Hooks.LogHook ( logger
                     , statusBar
                     )
import Hooks.LayoutHook ( layout
                        , fullscreenCustom
                        )
import Hooks.ManageHook (windowManager)
import Hooks.StartupHook (starter)
import Hooks.UrgencyHook (LibUrgencyHook (LibUrgencyHook))
import Hooks.HandleEventHook (handler)

xConfig = def { terminal = term
              , borderWidth = 2
              , keys = mappings
              , logHook = logger
              , modMask = mod4Mask
              , workspaces = spaces
              , layoutHook = layout
              , startupHook = starter
              , mouseBindings = mouse
              , handleEventHook = handler
              , manageHook = windowManager
              , clickJustFocuses = clickFocus
              , focusFollowsMouse = focusMouse
              , normalBorderColor = normalColor
              , focusedBorderColor = focusedColor
              }

main :: IO ()
main = xmonad
        . dynamicProjects projects
        . docks
        . ewmhFullscreen
        . ewmh
        . modal modes
        . fullscreenCustom
        . withUrgencyHook LibUrgencyHook
        . withSB statusBar
        $ xConfig

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
