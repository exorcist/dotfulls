{-# LANGUAGE FlexibleContexts #-}

{- |
   Module : Debug.Debug
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Debug.Debug ( notify
                   , getLayoutDesc
                   ) where

import Prelude ( (.)
               , (<$>)
               , String ()
               , concat
               )

import Control.Monad.State (MonadState ())
import Control.Monad.IO.Class (MonadIO ())

import XMonad ( XState ()
              , gets
              , windowset
              , description
              )
import XMonad.StackSet ( layout
                       , current
                       , workspace
                       )

import App.Alias (spawnList)

notify :: MonadIO m => String -> String -> m ()
notify title message = spawnList [ "notify-send"
                                 , "-u"
                                 , "low"
                                 , concat ["'", title, "'"]
                                 , concat ["'", message, "'"]
                                 ]

getLayoutDesc :: MonadState XState m => m String
getLayoutDesc = description . layout . workspace . current <$> gets windowset

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
