{- |
   Module : App.Scratchpad
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module App.Scratchpad ( fmSP
                      , ghciSP
                      , mailSP
                      , ocamlSP
                      , musicSP
                      , terminalSP
                      , scratchpads
                      ) where

import Prelude ( ($)
               , (/)
               , (-)
               , (<>)
               , Bool ()
               , String ()
               , Rational ()
               )

import Data.List (unwords)

import System.FilePath ((</>))

import XMonad ( (=?)
              , Query ()
              , ManageHook ()
              , stringProperty
              )
import XMonad.StackSet (RationalRect (RationalRect))

import XMonad.Util.NamedScratchpad ( NamedScratchpad ( NS
                                                     , cmd
                                                     , hook
                                                     , name
                                                     , query
                                                     )
                                   , customFloating
                                   )

import App.Alias ( term
                 , mail
                 , files
                 , music
                 , configHome
                 )

fmSP, ghciSP, mailSP, ocamlSP, musicSP, terminalSP :: String
fmSP = "FFF"
ghciSP = "GHCI"
mailSP = "Mail"
ocamlSP = "OCaml"
musicSP = "Music"
terminalSP = "Terminal"

isRole :: Query String
isRole = stringProperty "WM_WINDOW_ROLE"

scratchpads :: [NamedScratchpad]
scratchpads = [ NS { name = ghciSP
                   , query = isGHCI
                   , cmd = spawnGHCI
                   , hook = centerFloat
                   }
              , NS { name = ocamlSP
                   , query = isOcaml
                   , cmd = spawnOcaml
                   , hook = centerFloat
                   }
              , NS { name = musicSP
                   , query = isMusic
                   , cmd = spawnMusic
                   , hook = centerFloat
                   }
              , NS { hook = topFloat
                   , name = terminalSP
                   , query = isTerminal
                   , cmd = spawnTerminal
                   }
              , NS { name = fmSP
                   , query = isFM
                   , cmd = spawnFM
                   , hook = centerFloat
                   }
              , NS { name = mailSP
                   , query = isMail
                   , cmd = spawnMail
                   , hook = mailFloat
                   }
              ] where
                  spawnFM, spawnGHCI, spawnMail, spawnOcaml, spawnMusic, spawnTerminal :: String
                  spawnFM = unwords [term, "--role=" <> fmSP, "-e", files]
                  spawnGHCI = unwords [term, "--role=" <> ghciSP, "-e", "'stack exec -- ghci", "-v0", "-ghci-script", configHome </> "ghci/ghci.conf'"]
                  spawnMail = unwords [term, "--role=" <> mailSP, "-e", mail]
                  spawnOcaml = unwords [term, "--role=" <> ocamlSP, "-e", "myutop"]
                  spawnMusic = unwords [term, "--role=" <> musicSP, "-e", music]
                  spawnTerminal = unwords [term, "--role=" <> terminalSP, "-e", "tmux"]

                  isFM, isGHCI, isMail, isOcaml, isMusic, isTerminal :: Query Bool
                  isFM = isRole =? fmSP
                  isGHCI = isRole =? ghciSP
                  isMail = isRole =? mailSP
                  isOcaml = isRole =? ocamlSP
                  isMusic = isRole =? musicSP
                  isTerminal = isRole =? terminalSP

                  topFloat, mailFloat, centerFloat :: ManageHook
                  topFloat = customFloating $ RationalRect 0 0 1 0.45
                  mailFloat = customFloating $ RationalRect x y w h where
                      x, y, w, h :: Rational
                      w = 1/1.5
                      h = 1
                      x = (1 - w) / 2
                      y = (1 - h) / 2
                  centerFloat = customFloating $ RationalRect x y w h where
                      x, y, w, h :: Rational
                      w = 1/1.5
                      h = 1/1.5
                      x = (1 - w) / 2
                      y = (1 - h) / 2


-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
