{- |
   Module : App.Projects
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module App.Projects ( at
                    , spaces
                    , projects
                    ) where

import Prelude ( ($)
               , (.)
               , (!!)
               , (<$>)
               , Eq ()
               , Int ()
               , Enum ()
               , Show ()
               , Maybe ( Just
                       , Nothing
                       )
               , String ()
               , Bounded ()
               , show
               , pred
               , minBound
               , enumFrom
               )

import Data.Char (toLower)

import System.FilePath ((</>))

import XMonad.Actions.SpawnOn (spawnHere)
import XMonad.Actions.DynamicProjects ( Project (Project)
                                      , projectName
                                      , projectStartHook
                                      , projectDirectory
                                      )

import App.Alias ( (<..>)
                 , term
                 , files
                 , script
                 , inTerm
                 , browser
                 , homeDir
                 , soulseek
                 , configDir
                 , dataDrive
                 , andExecute
                 )

data Workspaces = ONE
                | TWO
                | THREE
                | FOUR
                | FIVE
                | SIX
                | SEVEN
                | EIGHT
                | NINE
                | TEN deriving (Eq, Show, Enum, Bounded)

at :: Int -> String
at = (spaces !!) . pred

spaces :: [String]
spaces = projectName <$> projects

at' :: Int -> String
at' = (projectNames !!) . pred

projectNames :: [String]
projectNames = toLower <..> (show <$> (enumFrom minBound :: [Workspaces]))

projects :: [Project]
projects = [ Project { projectName = at' 1
                     , projectDirectory = homeDir
                     , projectStartHook = Just . spawnHere $ browser
                     }
           , Project { projectName = at' 2
                     , projectDirectory = homeDir
                     , projectStartHook = Nothing
                     }
           , Project { projectName = at' 3
                     , projectDirectory = dataDrive </> "Movies"
                     , projectStartHook = Just . spawnHere . inTerm . andExecute $ files
                     }
           , Project { projectName = at' 4
                     , projectDirectory = configDir
                     , projectStartHook = Just $ do
                                        spawnHere term
                                        spawnHere term
                     }
           , Project { projectName = at' 5
                     , projectDirectory = homeDir
                     , projectStartHook = Nothing
                     }
           , Project { projectName = at' 6
                     , projectDirectory = homeDir
                     , projectStartHook = Just . spawnHere $ "filezilla"
                     }
           , Project { projectName = at' 7
                     , projectDirectory = homeDir
                     , projectStartHook = Just . spawnHere $ soulseek
                     }
           , Project { projectName = at' 8
                     , projectDirectory = homeDir
                     , projectStartHook = Nothing
                     }
           , Project { projectName = at' 9
                     , projectDirectory = homeDir
                     , projectStartHook = Nothing
                     }
           , Project { projectName = at' 10
                     , projectDirectory = homeDir
                     , projectStartHook = Just . spawnHere $ script "mc"
                     }
           ]

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
