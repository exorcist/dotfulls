{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts #-}

{- |
   Module : App.Alias
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module App.Alias ( (+++)
                 , (<..>)
                 , (<$$>)
                 , fst
                 , snd
                 , bar
                 , zip3
                 , fst3
                 , snd3
                 , thd3
                 , fst4
                 , snd4
                 , thd4
                 , fth4
                 , term
                 , mail
                 , htop
                 , files
                 , music
                 , editor
                 , binDir
                 , inTerm
                 , script
                 , unList
                 , getEnv'
                 , browser
                 , homeDir
                 , dataDir
                 , dataHome
                 , inEditor
                 , soulseek
                 , cacheDir'
                 , logToFile
                 , configDir
                 , spawnList
                 , termFloat
                 , inBrowser
                 , cacheHome
                 , dataDrive
                 , termInDir
                 , removeHash
                 , focusMouse
                 , clickFocus
                 , browserAlt
                 , configHome
                 , andExecute
                 , andHasClass
                 , imageViewer
                 , normalColor
                 , focusedColor
                 , inWorkingDir
                 , inImageViewer
                 ) where

import Prelude ( ($)
               , (.)
               , (<>)
               , (<$>)
               , Bool ( False
                      , True
                      )
               , String ()
               , Functor ()
               , const
               , foldl1
               , appendFile
               )

import Control.Monad.IO.Class ( MonadIO ()
                              , liftIO
                              )

import Data.List (unwords)

import System.IO.Unsafe (unsafeDupablePerformIO)

import System.FilePath ((</>))
import System.Environment (getEnv)

import XMonad ( X ()
              , spawn
              )

import Theme.Theme ( base12
                   , base18
                   )

(+++) :: String -> String -> String
a +++ b = a <> [' '] <> b

(<..>) :: Functor f => (a -> b) -> f (f a) -> f (f b)
(<..>) = (<$>) . (<$>)

(<$$>) :: Functor f => f (a -> b) -> a -> f b
f <$$> v = ($ v) <$> f

fst :: (a, b) -> a
fst (a, _) = a

snd :: (a, b) -> b
snd (_, b) = b

fst3 :: (a, b, c) -> a
fst3 (a, _, _) = a

snd3 :: (a, b, c) -> b
snd3 (_, b, _) = b

thd3 :: (a, b, c) -> c
thd3 (_, _, c) = c

fst4 :: (a, b, c, d) -> a
fst4 (a, _, _, _) = a

snd4 :: (a, b, c, d) -> b
snd4 (_, b, _, _) = b

thd4 :: (a, b, c, d) -> c
thd4 (_, _, c, _) = c

fth4 :: (a, b, c, d) -> d
fth4 (_, _, _, d) = d

zip3 :: [a] -> [b] -> [c] -> [(a, b, c)]
zip3 (x:xs) (y:ys) (z:zs) = (x,y,z) : zip3 xs ys zs
zip3 [] _ _ = []
zip3 _ [] _ = []
zip3 _ _ [] = []

removeHash :: String -> String
removeHash ('#':color) = color
removeHash x = x

unList :: [a] -> a
unList = foldl1 const

logToFile :: MonadIO m => String -> m ()
logToFile x = liftIO $ appendFile (dataDir </> "xmonad.log") (x <> "\n")

spawnList :: MonadIO m => [String] -> m ()
spawnList = spawn . unwords

focusMouse, clickFocus :: Bool
focusMouse = True
clickFocus = False

normalColor, focusedColor :: String
normalColor = base18
focusedColor = base12

script :: String -> String
script = (binDir </>)

getEnv' :: String -> String
getEnv' = unsafeDupablePerformIO . getEnv

soulseek :: String = script "soulseek"

browserAlt :: String = unwords [ "qutebrowser"
                               , "--qt-flag", "ignore-gpu-blacklist"
                               , "--qt-flag", "num-raster-threads=2"
                               , "--qt-flag", "enable-gpu-rasterization"
                               , "--qt-flag", "enable-oop-rasterization"
                               , "--qt-flag", "enable-native-gpu-memory-buffers"
                               ]
bar :: String = getEnv' "BAR"
term :: String = getEnv' "TERMINAL"
mail :: String = getEnv' "MAIL"
htop :: String = getEnv' "SYSMON"
music :: String = getEnv' "MUSIC"
files :: String = getEnv' "FM"
binDir :: String = homeDir </> ".local" </> "bin"
editor :: String = getEnv' "EDITOR"
browser :: String = getEnv' "BROWSER"
homeDir :: String = getEnv' "HOME"
dataDir :: String = getEnv' "XMONAD_DATA_HOME"
dataHome :: String = getEnv' "XDG_DATA_HOME"
cacheDir' :: String = getEnv' "XMONAD_CACHE_HOME"
cacheHome :: String = getEnv' "XDG_CACHE_HOME"
configDir :: String = getEnv' "XMONAD_CONFIG_HOME"
dataDrive :: String = getEnv' "DATA"
configHome :: String = getEnv' "XDG_CONFIG_HOME"
imageViewer :: String = getEnv' "IMAGE"

inTerm :: String -> String = (term +++)
inBrowser :: String -> String = (browser +++)
termInDir :: String -> String = inTerm . inWorkingDir
andExecute :: String -> String = ("-e" +++)
andHasClass :: String -> String = ("--role=" <>)
inWorkingDir :: String -> String = ("--cwd=" <>)
inImageViewer :: String -> String = (imageViewer +++)

inEditor :: String -> String
inEditor f = inTerm . andExecute $ editor +++ f

termFloat :: X ()
termFloat = spawnList [term, "--role=" <> "tym-float"]

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
