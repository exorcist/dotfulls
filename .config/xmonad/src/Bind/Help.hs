{- |
   Module : Bind.Help
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Bind.Help (showHelp) where

import Prelude ( ($)
               , (.)
               , (<>)
               , (<$>)
               , String ()
               , show
               )

import Data.List (unlines)

import Control.Monad.IO.Class ( MonadIO ()
                              , liftIO
                              )

import System.IO (writeFile)
import System.FilePath ((</>))

import App.Alias ( (+++)
                 , fst
                 , snd
                 , term
                 , editor
                 , cacheDir'
                 , configDir
                 , spawnList
                 )

showHelp :: MonadIO m => m ()
showHelp = do let file :: String
                  file = cacheDir' </> "keybinds.txt"

              liftIO . writeFile file $ unlines entryText <> parseHelp help <> unlines endText
              spawnList [term, "-c", "help", "-e", editor, "-c", show "nnoremap q ZQ", "-R", file]

entryText :: [String]
entryText = ["Press q to quit.", [], "Welcome to my custom XMonad build, here are the keybinds for it:", []]

endText :: [String]
endText = [[], "For more keybinds open" +++ configDir </> "src/Bind/KeyBoard.hs"]

parseHelp :: [(String, String)] -> String
parseHelp xs = unlines $ (((<> ": ") <$> fst) <> snd) <$> xs

help :: [(String, String)]
help = [ ("Super + b", "Toggle the state of the bar between hidden and shown")
       , ("Super + c", "Focus the next window in the current workspace")
       , ("Super + d", "Pick a workspace to move the focused window to")
       , ("Super + a e", "Open the neomutt scratchpad")
       , ("Super + e", "Open Terminal")
       , ("Super + a f", "Open the file manager scratchpad")
       , ("Super + f", "Toggle the state of the focused window between fullscreen and tiled")
       , ("Super + g", "Open the ghci scratchpad")
       , ("Super + h", "Move focus to the window left of the currently focused window")
       , ("Super + i", "Untab the focused window and go to the next one")
       , ("Super + j", "Move focus to the window down of the currently focused window")
       , ("Super + k", "Move focus to the window up of the currently focused window")
       , ("Super + l", "Move focus to the window right of the currently focused window")
       , ("Super + a m", "Open the music scratchpad")
       , ("Super + n (d, l)", "Switch the colorscheme from dark to light and vice-versa")
       , ("Super + a o", "Open the utop scratchpad")
       , ("Super + o", "Display prompt search engine options")
       , ("Super + p", "Display other prompts options")
       , ("Super + q", "Recompile and restart XMonad")
       , ("Super + r", "Rotate the windows dependent on the layout")
       , ("Super + s", "Toggle the state of the focused window between floating and tiled")
       , ("Super + t", "Tab the focused window and the one after it")
       , ("Super + u", "Pick an emoji and copy it to clipboard")
       , ("Super + a t", "Open a terminal scratchpad running tmux")
       , ("Super + w", "Kill the focused window")
       , ("Super + x", "Open the treeselect menu")
       , ("Super + y", "Toggle the dynamic scratchpad")
       , ("Super + z", "Open qutebrowser")
       , ("Super + return", "Repeat the previous keybind")
       , ("Super + space", "Go to the next layout")
       , ("Super + comma", "Increase the stack size by one window")
       , ("Super + period", "Decrease the stack size by one window")
       , ("Super + backspace", "Download the copied url if it's and image and put it in the wallpaper directory")
       , ("Super + minus", "Set a random wallpaper from the wallpapers folder")
       , ("Super + F2", "Download the copied url if it's music and put it in the music folder")
       , ("Super + F3", "Pick a device and an image to flash to that device")
       , ("Super + F4", "Toggle night light on or off")
       , ("Super + left bracket", "Pick a window to go to")
       , ("Super + right bracket", "Pick a window to bring to the current workspace")
       , ("Super + backslash", "Toggle Resize Mode")
       , ("Super + semicolon", "Focus the previous tab")
       , ("Super + apostrophe", "Focus the next tab")
       , ("Super + Shift + b", "Show and hide the borders of all windows in the current workspace")
       , ("Super + Shift + c", "Focus the previous window")
       , ("Super + Shift + d", "Focus the master window")
       , ("Super + Shift + f", "Select a window to tab the focused one with")
       , ("Super + Shift + i", "Untab all windows in the current workspace")
       , ("Super + Shift + k", "Pick a window to kill")
       , ("Super + Shift + q", "Ask whether to quit XMonad")
       , ("Super + Shift + r", "Select a rectangle to record")
       , ("Super + Shift + w", "Ask whether to close all windows in the current workspace")
       , ("Super + Shift + slash", "Open a notification showing the currently playing song")
       , ("Super + Shift + minus", "Decrease the gap size by 3")
       , ("Super + Shift + equal", "Increase the gap size by 3")
       , ("Super + Shift + space", "Reset the layout to the default one on all workspaces")
       , ("Super + Control + b", "Go back in the song by 5 (percent?/seconds?)")
       , ("Super + Control + d", "Enable the float mode")
       , ("Super + Control + f", "Go forward in the song by 5 (percent?/seconds?)")
       , ("Super + Control + g", "Enable nomod mode")
       , ("Super + Control + n", "Enable the noclose mode")
       , ("Super + Control + r", "Enable the resize mode")
       , ("Super + Control + t", "Toggle the window hints on or off on the current workspace")
       , ("Super + Control + u", "Tabs and untabs all windows in the current workspace")
       , ("Super + Control + w", "Use xkill to kill the focused window")
       , ("Super + Control + minus", "Decrease the spacing on the current workspace by 1")
       , ("Super + Control + equal", "Increase the spacing on the current workspace by 1")
       , ("Super + Control + space", "Reset the layout only on the current workspace")
       , ("Super + Control + left bracket", "Toggle the compositor on or off")
       , ("Super + Control + Shift + space", "Apply the layout set to the current workspace to all workspaces")
       , ("Super + Alt + space", "Refresh")
       , ("Super + Alt + f", "Search the currently playing song (mpd)")
       , ("Super + Alt + l", "Save the current windowset to $XMONAD_DATA_HOME/xmonad.log")
       , ("Super + Alt + o", "Open selected url in mpv")
       , ("Super + Alt + y", "Toggle copy of the selected window to all workspaces")
       , ("Super + Alt + return", "Pick a program to open")
       , ("XF86AudioMute", "Mute the sound")
       , ("XF86AudioPlay", "Play the music")
       , ("XF86AudioPause", "Pause the music")
       , ("XF86AudioPrev", "Play the previous song")
       , ("XF86AudioNext", "Play the next song")
       , ("XF86AudioStop", "Stop playing")
       , ("XF86AudioLowerVolume", "Lower the playback volume by 5%")
       , ("XF86AudioRaiseVolume", "Raise the playback volume by 5%")
       , ("Alt + b", "Open the browser set by the $BROWSER variable")
       , ("Alt + c", "Select a rectangle to open a floating terminal in")
       , ("Alt + f", "Open the file manager set by the $FM variable")
       , ("Alt + g", "Open this help screen")
       , ("Alt + h", "Open the system monitor set by the $SYSMON variable")
       , ("Alt + m", "Open the mail client set by the $MAIL variable")
       , ("Alt + n", "Open the music player set by the $MUSIC variable")
       , ("Alt + o (d, t, s, n, f, g)", "Open discord, signal, telegram, soulseek, filezilla or gimp depending on the key pressed")
       , ("Alt + p", "Open pulsemixer")
       , ("Alt + s", "Select an area to take a screenshot of")
       , ("Alt + v", "Open virt-manager")
       , ("Alt + w", "Open the image viewer set by the $IMAGE variable in the wallpapers folder")
       , ("Alt + x", "Select a color and copy it to the clipboard")
       , ("Alt + Shift + c", "Opens a floating terminal with the selected dimensions and program running inside")
       , ("Print", "Take a fullscreen screenshot")
       , ("Control + m", "Make the focused window the master")
       , ("Control + semicolon", "Move the focused tab up")
       , ("Control + apostrophe", "Move the focused tab down")
       , ("Control + escape", "Toggle the state of the microphone between muted and unmuted")
       , ("Control + Shift + l", "Lock the screen")
       ]
