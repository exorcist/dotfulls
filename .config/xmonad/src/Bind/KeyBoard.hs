{-# LANGUAGE LambdaCase #-}

{- |
   Module : Bind.KeyBoard
   Copyright : (c) 2020, 2021, 2022, 2023, 2024 Joan Milev <joantmilev@gmail.com>
   License : MIT

   Maintainer : Joan Milev <joantmilev@gmail.com>
   Stability : Stable
   Portability : Unknown
-}

module Bind.KeyBoard ( modes
                     , mappings
                     ) where

-- {{{

import Prelude ( ($)
               , (.)
               , (/)
               , (==)
               , (/=)
               , (<>)
               , (!!)
               , (*>)
               , (<$>)
               , (>>=)
               , Int ()
               , Num ( (+)
                     , (-)
                     )
               , Bool ( True
                      , False
                      )
               , Maybe (Just)
               , Either (Right)
               , String ()
               , Functor ()
               , id
               , zip
               , head
               , show
               , pure
               , const
               , concat
               , unwords
               , reverse
               )

import Data.Map ( Map ()
                , member
                , lookup
                , toList
                )
import Data.List ( drop
                 , filter
                 , isInfixOf
                 )
import Data.Bifunctor ( Bifunctor ()
                      , second
                      )

import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)

import System.Exit (exitSuccess)
import System.FilePath ((</>))

import Network.MPD ( Song (Song)
                   , next
                   , stop
                   , toggle
                   , withMPD
                   , seekCur
                   , currentSong
                   )

import XMonad ( X ()
              , Window ()
              , Message ()
              , KeySym ()
              , Resize ( Expand
                       , Shrink
                       )
              , ButtonMask ()
              , XConfig (layoutHook)
              , IncMasterN (IncMasterN)
              , SomeMessage (SomeMessage)
              , ChangeLayout (NextLayout)
              , LayoutMessages (ReleaseResources)
              , gets
              , asks
              , spawn
              , config
              , unGrab
              , restart
              , windows
              , whenJust
              , setLayout
              , recompile
              , windowset
              , killWindow
              , sendMessage
              , withFocused
              , withWindowSet
              , handleMessage
              , withUnfocused
              , getDirectories
              , broadcastMessage
              )
import XMonad.StackSet ( Screen (Screen)
                       , StackSet (StackSet)
                       , RationalRect (RationalRect)
                       , sink
                       , float
                       , swapUp
                       , layout
                       , hidden
                       , current
                       , visible
                       , focusUp
                       , focusUp'
                       , floating
                       , swapDown
                       , workspace
                       , focusDown
                       , focusDown'
                       , swapMaster
                       , focusMaster, currentTag
                       )

import XMonad.Actions.Search ( SearchEngine ()
                             , imdb
                             , search
                             , hoogle
                             , amazon
                             , youtube
                             , wayback
                             , stackage
                             , cratesIo
                             , wikipedia
                             , duckduckgo
                             , vocabulary
                             , clojureDocs
                             , promptSearch
                             , searchEngine
                             , searchEngineF
                             )
import XMonad.Actions.WithAll (killAll)
import XMonad.Actions.CycleWS ( nextWS
                              , prevWS
                              , toggleWS'
                              )
import XMonad.Actions.TreeSelect (treeselectAction)
import XMonad.Actions.CopyWindow ( copy
                                 , copyToAll
                                 , wsContainingCopies
                                 , killAllOtherCopies
                                 )
import XMonad.Actions.EasyMotion (selectWindow)
import XMonad.Actions.GridSelect ( goToSelected
                                 , bringSelected
                                 )
import XMonad.Actions.RepeatAction (rememberActions)
import XMonad.Actions.Navigation2D ( windowGo
                                   , windowSwap
                                   )
import XMonad.Actions.DynamicProjects ( renameProjectPrompt
                                      , switchProjectPrompt
                                      , shiftToProjectPrompt
                                      , changeProjectDirPrompt
                                      )
import XMonad.Actions.PerWorkspaceKeys (bindOn)

import XMonad.Hooks.Modal ( Mode ()
                          , mode
                          , overlay
                          , setMode
                          , mkKeysEz
                          , floatMode
                          , noModMode
                          , modeWithExit
                          , noModModeLabel
                          )
import XMonad.Hooks.StatusBar ( killStatusBar
                              , spawnStatusBar
                              )
import XMonad.Hooks.ManageDocks ( Direction2D ( U
                                              , D
                                              , R
                                              , L
                                              )
                                , ToggleStruts (ToggleStruts)
                                )

import XMonad.Layout.Gaps ( GapMessage ( IncGap
                                       , DecGap
                                       , ModifyGaps
                                       )
                          )
import XMonad.Layout.Spacing ( incScreenWindowSpacing
                             , decScreenWindowSpacing
                             )
import XMonad.Layout.ResizableTile ( MirrorResize ( MirrorExpand
                                                  , MirrorShrink
                                                  )
                                   )
import XMonad.Layout.SubLayouts ( GroupMsg ( UnMerge
                                           , MergeAll
                                           , UnMergeAll
                                           )
                                , toSubl
                                , onGroup
                                , mergeDir
                                , pushGroup
                                )
import XMonad.Layout.MultiToggle (Toggle (Toggle))
import XMonad.Layout.MultiToggle.Instances (StdTransformers (NOBORDERS))

import XMonad.Prompt (XPConfig ())
import XMonad.Prompt.Man (manPrompt)
import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Prompt.Unicode (mkUnicodePrompt)
import XMonad.Prompt.ConfirmPrompt (confirmPrompt)

import XMonad.Util.PureX ( view
                         , shift
                         , defile
                         , handlingRefresh
                         )
import XMonad.Util.EZConfig (mkKeymap)
import XMonad.Util.XSelection (getSelection)
import XMonad.Util.ActionCycle (cycleAction)
import XMonad.Util.NamedScratchpad ( dynamicNSPAction
                                   , toggleDynamicNSP
                                   , namedScratchpadAction
                                   )

import App.Alias ( (+++)
                 , snd
                 , term
                 , mail
                 , htop
                 , files
                 , music
                 , inTerm
                 , script
                 , browser
                 , dataHome
                 , termFloat
                 , logToFile
                 , spawnList
                 , browserAlt
                 , andExecute
                 , inImageViewer
                 )
import App.Projects (spaces)
import App.Scratchpad ( fmSP
                      , ghciSP
                      , mailSP
                      , ocamlSP
                      , musicSP
                      , terminalSP
                      , scratchpads
                      )

import Bind.Help (showHelp)

import Config.GridSelect (gsConfig)
import Config.TreeSelect ( treeConfig
                         , treeActions
                         )

import Config.Prompts ( promptTheme
                      , noHistTheme
                      , unicodeTheme
                      , criticalTheme
                      , torrentPrompt
                      , appImagePrompt
                      , uploadFilePrompt
                      , termLaunchPrompt
                      )

import Debug.Debug ( notify
                   , getLayoutDesc
                   )

import Hooks.LayoutHook ( ToggleHints (ToggleHints)
                        , ToggleCentered (ToggleCentered)
                        , emConfig
                        )

-- End Imports }}}

mappings :: XConfig l -> Map (ButtonMask, KeySym) (X ())
mappings c = mkKeymap c
             . rememberActions "M4-<Return>"
             $ concat [ launchKeys
                      , layoutKeys
                      , mediaCntrl
                      , othersKeys
                      , promptKeys
                      , windowKeys
                      ]

launchKeys, layoutKeys, mediaCntrl, othersKeys, promptKeys, windowKeys :: [(String, X ())]
launchKeys = [ ("M4-<Backspace>", unGrab *> spawn (script "wl" +++ "clip"))
             , ("M4-e", spawn term)
             , ("M4-n d", spawn $ script "switch" +++ "d")
             , ("M4-n l", spawn $ script "switch" +++ "l")
             , ("M4-x", treeselectAction treeConfig treeActions)
             , ("M4-z", spawn browserAlt)
             , ("M4-C--", spawn $ script "wl" +++ "random")
             , ("M4-S-r", unGrab *> spawn (script "record"))
             , ("M4-S-/", spawn $ script "music" +++ "current")
             , ("M4-C-[", spawn $ script "pc")
             , ("M1-b", spawn browser)
             , ("M1-c", termFloat)
             , ("M1-m", spawn . inTerm . andExecute $ mail)
             , ("M1-n", spawn . inTerm . andExecute $ music)
             , ("M1-h", spawn . inTerm . andExecute $ htop)
             , ("M1-f", spawn . inTerm . andExecute $ files)
             , ("M1-p", spawn . inTerm . andExecute $ "pulsemixer")
             , ("M1-s", spawn $ script "scr" +++ "sel")
             , ("M1-v", spawn "virt-manager")
             , ("M1-w", spawn . inImageViewer . ("-rt" +++) $ dataHome </> "src" </> "wallpapers")
             , ("M1-x", do
                 unGrab
                 spawnList ["xcolor"
                           , "|"
                           , "tr -d '\n'"
                           , "|"
                           , "xsel -b -i"
                           , "&&"
                           , "notify-send"
                           , "-u"
                           , "low"
                           , "xcolor"
                           , "'Color copied to clipboard!'"
                           ]
               )
             , ("C-<Escape>", spawn $ script "vol" +++ "toggle_mic")
             , ("C-S-l", unGrab *> spawnList ["xset", "s", "activate"])
             ]
layoutKeys = [ ("M4-<Space>", sendMessage NextLayout)
             , ("M4-b", sendMessageAll ToggleStruts *> cycleAction "bar" [ killStatusBar "xmobar"
                                                                         , spawnStatusBar "xmobar"
                                                                         ]
               )
             , ("M4-f", toggleFullscreen)
             , ("M4-i", withFocused $ sendMessage . UnMerge)
             , ("M4-s", withFocused toggleFloat)
             , ("M4-t", withFocused $ sendMessage . mergeDir id)
             , ("M4-S-.", nextWS)
             , ("M4-S-,", prevWS)
             , ("M4-,", getLayoutDesc >>= \case
                    "full" -> pure ()
                    _ -> sendMessage $ IncMasterN 1
               )
             , ("M4-.", getLayoutDesc >>= \case
                    "full" -> pure ()
                    _ -> sendMessage . IncMasterN $ -1
               )
             , ("M4--", getLayoutDesc >>= \case
                    "full" -> pure ()
                    _ -> sendMessage . ModifyGaps $ resizeGaps (\x -> x - 3)
               )
             , ("M4-=", getLayoutDesc >>= \case
                    "full" -> pure ()
                    _ -> sendMessage . ModifyGaps $ resizeGaps (+ 3)
               )
             , ("M4-C-u", cycleAction "tabAll" [ withFocused $ sendMessage . MergeAll
                                               , withFocused $ sendMessage . UnMergeAll
                                               ]
               )
             , ("M4-S-<Space>", resetLayout *> setAllLayout)
             , ("M4-C-<Space>", resetLayout)
             , ("M4-M1-<Space>", toSubl NextLayout)
             , ("M4-S-b", sendMessage $ Toggle NOBORDERS)
             , ("M4-S-i", withFocused $ sendMessage . UnMergeAll)
             , ("M4-C-S-<Space>", setAllLayout)
             , ("M4-C-t", sendMessage $ Toggle ToggleHints)
             , ("M4-C-q", sendMessage $ Toggle ToggleCentered)
             , ("M4-S-=", incScreenWindowSpacing 1)
             , ("M4-S--", decScreenWindowSpacing 1)
             , ("M4-M1-y", toggleCopyToAll)
             ]
mediaCntrl = [ ("<XF86AudioMute>", spawnList [script "vol", "mute"])
             , ("<XF86AudioPlay>", liftIO . void $ withMPD toggle)
             , ("<XF86AudioPrev>", spawnList [script "other/sop"])
             , ("<XF86AudioNext>", liftIO . void $ withMPD next)
             , ("<XF86AudioStop>", liftIO . void $ withMPD stop)
             , ("<XF86AudioLowerVolume>", spawnList [script "vol", "-"])
             , ("<XF86AudioRaiseVolume>", spawnList [script "vol", "+"])
             , ("<XF86MonBrightnessUp>", spawnList ["xbacklight", "-inc", "10"])
             , ("<XF86MonBrightnessDown>", spawnList ["xbacklight", "-dec", "10"])
             , ("M4-C-b", liftIO . void . withMPD $ seekCur False (-5))
             , ("M4-C-f", liftIO . void . withMPD $ seekCur False 5)
             -- , ("<Print>", unGrab *> spawn (script "scr" +++ "full"))
             , ("S-<Print>", unGrab *> spawn (script "scr" +++ "show"))
             , ("M4-M1-f", findCurrentSong)
             , ("M4-M1-o", getSelection >>= \sel -> spawnList [script "open", sel])
             ]
othersKeys = [ ("<XF86Tools>", namedScratchpadAction scratchpads musicSP)
             , ("M4-a g", namedScratchpadAction scratchpads ghciSP)
             , ("M4-a m", namedScratchpadAction scratchpads musicSP)
             , ("M4-a t", namedScratchpadAction scratchpads terminalSP)
             , ("M4-a f", namedScratchpadAction scratchpads fmSP)
             , ("M4-a o", namedScratchpadAction scratchpads ocamlSP)
             , ("M4-a e", namedScratchpadAction scratchpads mailSP)
             , ("M4-C-d", setMode "float")
             , ("M4-C-g", setMode noModModeLabel)
             , ("M4-C-n", setMode "noclose")
             , ("M4-C-r", setMode "resize")
             , ("M1-S-d", getLayoutDesc >>= notify "xmonad")
             , ("M1-S-m", spawnList ["systemctl", "--user", "restart", "mpd"])
             ]
promptKeys = [ ("M4-u", unGrab *> mkUnicodePrompt "xsel" ["-b", "-i"] "/usr/share/unicode/UnicodeData.txt" unicodeTheme)
             , ("M1-S-c", termLaunchPrompt noHistTheme)
             , ("<XF86Launch1>", termLaunchPrompt noHistTheme)
             , ("M4-S-q", unGrab *> confirmPrompt criticalTheme "quit?" quitWM)
             , ("M4-S-w", unGrab *> confirmPrompt criticalTheme "kill all?" killAll)
             , ("M4-M1-<Return>", unGrab *> shellPrompt noHistTheme)
             ]
windowKeys = [ ("M4-c", windows focusDown)
             , ("M4-q", confirmPrompt criticalTheme "recompile & restart?" $ do
                 void $ liftIO getDirectories >>= \xDirs -> recompile xDirs False
                 restart "xmonad" True
               )
             , ("M4-S-f", selectWindow emConfig >>= (`whenJust` sendMessage . mergeDir id))
             , ("C-q", pure ())
             , ("M1-g", showHelp)
             , ("M4-w", withFocused killWindow)
             , ("M4-y", dynamicNSPAction "dynsp")
             , ("M4-S-y", withFocused $ toggleDynamicNSP "dynsp")
             , ("M4-S-z", withFocused $ windows . sink)
             , ("M4-[", unGrab *> goToSelected (gsConfig 250))
             , ("M4-]", unGrab *> bringSelected (gsConfig 250))
             , ("M4-;", bindOn [("Tabs", windows focusUp), ("", onGroup focusUp')])
             , ("M4-'", bindOn [("Tabs", windows focusDown), ("", onGroup focusDown')])
             , ("M4-S-c", windows focusUp)
             , ("M4-S-d", windows focusMaster)
             , ("M4-C-w", withFocused $ \w -> spawnList ["xkill", "-id", show w])
             , ("M4-C-S-w", withUnfocused killWindow)
             , ("C-m", windows swapMaster)
             , ("M4-M1-l", notify "xmonad" "logging window state" *> withWindowSet (logToFile . show))
             , ("C-;", bindOn [("Tabs", windows swapUp), ("", windows swapUp)])
             , ("C-'", bindOn [("Tabs", windows swapDown), ("", windows swapDown)])
             ]
          <> [("M1-o" +++ k, a) | (k, a) <- [ ("g", spawn "gimp")
                                            , ("f", spawn "filezilla")
                                            , ("n", spawn "soulseekqt")
                                            , ("e", spawn "easyeffects")
                                            , ("s", spawn "signal-desktop")
                                            , ("t", spawn "telegram-desktop")
                                            , ("d", spawn $ script "other" </> "discocss")
                                            ]
             ]
          <> [("M4-o" +++ k, a) | (k, a) <- searchList $ promptSearch promptTheme]
          <> [("M4-p" +++ k, a) | (k, a) <- promptList promptTheme]
          <> concat [[ ("M4-" <> k, windowGo d False)
                     , ("C-" <> k, windowSwap d False)] | (k, d) <- zip dirKeys dirs]
          <> [("M4-M1-" <> k, windows $ copy i) | (k,i) <- zip workspaceKeys spaces]
          <> [("M4-C-" <> k, sendMessage $ pushGroup d) | (k, d) <- zip dirKeys dirs]
          <> [("M4-C-S-" <> k, sendMessage $ DecGap 5 d) | (k, d) <- zip arrowKeys dirs]
          <> [("M4-C-" <> k, sendMessage $ IncGap 5 d) | (k, d) <- zip (reverse arrowKeys) dirs]
          <> [ ("M4-" <> m <> k, do
             x <- gets $ currentTag . windowset
             if x == i then toggleWS' ["NSP"] else defile f) | (i, k) <- zip spaces workspaceKeys
                                                             , (m, f) <- [("", view i), ("S-", shift i <> view i)]
             ]
          <> [("M4-C-" <> k, spawnList ["mpc", "-q", "seek", i <> "%"])
                           | (k,i) <- zip (show <$> [0..9])
                                          $ show <$> ([0, 10, 20, 30, 40, 50, 60, 70, 80, 90] :: [Int])]

resizeMode :: Mode
resizeMode = mode "resize" $
    mkKeysEz [ ("h", getLayoutDesc >>= \case
                               "tall" -> sendMessage Shrink
                               _ -> pure ()
               )
             , ("j", getLayoutDesc >>= \case
                               "tall" -> sendMessage MirrorShrink
                               _ -> pure ()
               )
             , ("k", getLayoutDesc >>= \case
                               "tall" -> sendMessage MirrorExpand
                               _ -> pure ()
               )
             , ("l", getLayoutDesc >>= \case
                               "tall" -> sendMessage Expand
                               _ -> pure ()
               )
             ]

noCloseMode :: Mode
noCloseMode = modeWithExit "`" "noclose" $ mkKeysEz [("M4-w", pure ())]

modes :: [Mode]
modes = [ noModMode
        , overlay "resize" resizeMode
        , overlay "float" $ floatMode 10
        , overlay "noclose" noCloseMode
        ]

replaceSWP :: String -> String
replaceSWP = ((\c -> if c == ' ' then '+' else c) <$>)

rust, ebay, urban, tenor, reddit, github, archWiki, azLyrics, youtubeMusic, metalArchives :: SearchEngine
rust = searchEngine "rust" "https://doc.rust-lang.org/std/index.html?search="
ebay = searchEngine "ebay" "https://www.ebay.com/sch/i.html?_nkw="
urban = searchEngine "urban" "https://www.urbandictionary.com/define.php?term="
tenor = searchEngineF "tenor" (\s -> "https://tenor.com/search/"  <> s <> "-gifs")
reddit = searchEngine "reddit" "https://www.reddit.com/search/?q="
github = searchEngine "github" "https://github.com/search?q="
archWiki = searchEngine "archwiki" "https://wiki.archlinux.org/index.php?search="
azLyrics = searchEngine "azlyrics" "https://search.azlyrics.com/search.php?q="
youtubeMusic = searchEngineF "youtube music" $ \s -> "https://music.youtube.com/search?q=" <> replaceSWP s
metalArchives = searchEngineF "metal archives" $ \s -> "https://www.metal-archives.com/search?type=band_name&searchString=" <> replaceSWP s

searchList :: (SearchEngine -> a) -> [(String, a)]
searchList m = [ ("a", m archWiki)
               , ("c", m metalArchives)
               , ("d", m duckduckgo)
               , ("e", m ebay)
               , ("g", m github)
               , ("h", m hoogle)
               , ("i", m imdb)
               , ("l", m azLyrics)
               , ("m", m youtubeMusic)
               , ("r", m reddit)
               , ("s", m stackage)
               , ("t", m tenor)
               , ("v", m vocabulary)
               , ("u", m urban)
               , ("w", m wikipedia)
               , ("y", m youtube)
               , ("z", m amazon)
               , ("S-c", m cratesIo)
               , ("S-j", m clojureDocs)
               , ("S-r", m rust)
               , ("S-w", m wayback)
               ]

dirs :: [Direction2D]
dirs = [L, D, U, R]

dirKeys :: [String]
dirKeys = ["h", "j", "k", "l"]

arrowKeys :: [String]
arrowKeys = ["<L>", "<D>", "<U>", "<R>"]

quitWM :: X ()
quitWM = sendMessageAll ReleaseResources *> liftIO exitSuccess

sendMessageAll :: Message a => a -> X ()
sendMessageAll m = handlingRefresh $ broadcastMessage m

resizeGaps :: (Functor f, Bifunctor p) => (c -> d) -> f (p b c) -> f (p b d)
resizeGaps = (<$>) . second

workspaceKeys :: [String]
workspaceKeys = show <$> [1..9] <> [0]

findCurrentSong :: X ()
findCurrentSong = do
    Right (Just (Song _ sng _ _ _ _)) <- liftIO $ withMPD currentSong

    let song = snd <$> toList sng

        song' :: String
        song' = unwords $ (\x -> if "Value" `isInfixOf` x
                                 then drop 6 . filter (/= '"') $ x
                                 else x
                          ) <$> [show . head $ head song, show . head $ song !! 3]

    search browser ("https://music.youtube.com/search?q=" <>) (replaceSWP song')

promptList :: XPConfig -> [(String, X ())]
promptList theme = [ ("m", manPrompt theme)
                   , ("a", appImagePrompt theme)
                   , ("r", renameProjectPrompt theme)
                   , ("c", switchProjectPrompt theme)
                   , ("s", shiftToProjectPrompt theme)
                   , ("d", changeProjectDirPrompt theme)
                   , ("t", torrentPrompt theme)
                   , ("u", uploadFilePrompt theme)
                   ]

toggleFloat :: Window -> X ()
toggleFloat w = windows $ \s ->
    if member w $ floating s
       then sink w s
       else float w (RationalRect (1/6) (1/6) (2/3) (2/3)) s

toggleFullscreen :: X ()
toggleFullscreen = withWindowSet $ \ws ->
    withFocused $ \w -> do
        let fullRect = RationalRect 0 0 1 1
            isFullFloat = w `lookup` floating ws == Just fullRect
        windows $ if isFullFloat then sink w else float w fullRect

toggleCopyToAll :: X ()
toggleCopyToAll = wsContainingCopies >>= \case
        [] -> windows copyToAll
        _ -> killAllOtherCopies

resetLayout :: X ()
resetLayout = asks (layoutHook . config) >>= setLayout

setAllLayout :: X ()
setAllLayout = do
    ss@StackSet { current = c@Screen {workspace = ws}
                , visible = sVisible
                , hidden = sHidden
                } <- gets windowset
    void . handleMessage (layout ws) $ SomeMessage ReleaseResources
    ll <- gets $ layout . workspace . current . windowset
    windows $ const ss { current = c {workspace = ws {layout = ll}}
                       , visible = setSL ll sVisible
                       , hidden = setHL ll sHidden
                       } where
                           setHL l = ((\w -> w {layout = l}) <$>)
                           setSL l = ((\w -> w {workspace = (workspace w){layout = l}}) <$>)

-- vim:ft=haskell:foldmethod=marker:expandtab:ts=4:shiftwidth=4
