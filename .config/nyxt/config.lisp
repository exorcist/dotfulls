(define-configuration buffer
  ((default-modes
    (pushnew 'nyxt/mode/vi:vi-normal-mode %slot-value%))))

(define-configuration web-buffer
  ((default-modes
    (pushnew 'nyxt/mode/blocker:blocker-mode %slot-value%))))

(setf (uiop/os:getenv "WEBKIT_DISABLE_COMPOSITING_MODE") "1")

(define-configuration browser
  ((theme
    (make-instance 'theme:theme :background-color "#1d2021"
                   :action-color "#b8bb26"
                   :primary-color "#83a598"
                   :secondary-color "#d3869b"
                   :success-color "#98971a"
                   :warning-color "#cc241d"
                   :highlight-color "#fabd2f"
                   :codeblock-color "#689d6a"
                   :text-color "#ebdbb2"
                   :contrast-text-color "#1d2021"))))
