#! /bin/sh

rm -rf /tmp/polybar_mqueue.*

. "${XDG_CONFIG_HOME:-"$HOME/.config"}/polybar/scripts/colors"

pidof polybar >"/dev/null" 2>&1 && kill -9 "$(pidof polybar)" >"/dev/null" 2>&1
polybar bar -r -c "${XDG_CONFIG_HOME:-"$HOME/.config"}/polybar/config.ini" >"/dev/null" 2>&1 &
