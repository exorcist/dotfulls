[@@@warning "-10"]

open UTop;;
open LTerm;;
open LTerm_text;;
open LTerm_style;;
open LTerm_editor;;

let const : 'a -> 'a React.signal =
    function n -> Lwt_react.S.const n;;

let prompt = eval [
    B_bold true;
    B_fg green;
    S "λ ";
] |> const;;

let prompt_continue = eval [
    B_bold false;
    B_fg blue;
    S "| ";
] |> const;;

let main : unit = begin
    UTop.edit_mode := Vi;
    UTop.set_show_box true;
    UTop.prompt := prompt;
    UTop.profile = const Dark;
    UTop.set_auto_run_async true;
    UTop.history_file_name := None;
end;;
