require("corrections")
require("autocmd")
require("fm")
require("plugins")
require("options")
require("colors")
require("statusline")
require("mappings")

-- vim:ft=lua
