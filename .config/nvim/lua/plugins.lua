local fn = vim.fn
local opt = vim.opt
local cmd = vim.cmd

cmd("packadd! impatient.nvim")

local impatient, _ = pcall(require, "impatient")

if impatient then
    require("impatient")
end

local root = fn.fnamemodify("~/.local/share/nvim/repro", ":p")

for _, name in ipairs({ "config", "data", "state", "cache" }) do
    vim.env[("XDG_%s_HOME"):format(name:upper())] = root .. "/" .. name
end

local lazypath = root .. "/plugins/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "--single-branch",
        "https://github.com/folke/lazy.nvim.git",
        lazypath,
    })
end

opt.runtimepath:prepend(lazypath)

local border = ({ "┌", "─", "┐", "│", "┘", "─", "└", "│" })

local plugins = {
    ({
        "lewis6991/impatient.nvim",
        event = "BufReadPre"
    }),

    ({
        "morhetz/gruvbox",
        event = "BufReadPre"
    }),

    ({
        "nvim-treesitter/nvim-treesitter",
        cmd = "TSUpdate",
        event = "BufReadPre",
        config = [[require("treesitter")]],
    }),

    ({
        "arzg/vim-sh",
        event = "BufReadPre"
    }),

    ({
        "purescript-contrib/purescript-vim",
        ft = "purescript"
    }),

    ({
        "wlangstroth/vim-racket",
        ft = "racket"
    }),

    ({
        "mnacamura/vim-r7rs-syntax",
        ft = ({ "scheme", "r7rs", "r6rs" })
    }),

    ({
        "ziglang/zig.vim",
        ft = "zig"
    }),

    ({
        "tweekmonster/startuptime.vim",
        cmd = "StartupTime"
    }),

    ({
        "norcalli/nvim-colorizer.lua",
        event = "CursorHold"
    }),

    ({
        "gpanders/nvim-parinfer",
        ft = ({ "clojure", "fennel", "scheme", "racket", "lisp", "haskell" })
    }),

    ({
        "neovim/nvim-lspconfig",
        event = "BufReadPre",
        config = [[require("lspconfig")]]
    }),

    ({
        "windwp/nvim-autopairs",
        event = "InsertEnter",
        config = function ()
            require("nvim-autopairs").setup({
                disable_in_macro = true,
                enable_moveright = true,
                enable_afterquote = true,
                close_triple_quotes = true,
                enable_check_bracket_line = true
            })
        end
    }),

    ({
        "numToStr/Comment.nvim",
        event = "BufReadPre",
        config = function()
            require("Comment").setup({
                ignore = "^$",
                sticky = true,
                padding = true,
                toggler = ({
                    line = "gcc",
                    block = "gbc"
                }),
                opleader = ({
                    line = "gc",
                    block = "gb"
                }),
                extra = ({
                    above = "gcO",
                    below = "gco",
                    eol = "gcA"
                }),
                mappings = ({
                    basic = true,
                    extra = true
                }),
                pre_hook = nil,
                post_hook = nil
            })
        end
    }),

    ({
        "hrsh7th/nvim-cmp",
        event = "BufRead",
        config = [[require("completion")]],
        dependencies = ({
            ({
                "f3fora/cmp-spell",
                after = "nvim-cmp"
            }),
            ({
                "hrsh7th/cmp-path",
                after = "cmp-spell"
            }),
            ({
                "L3MON4D3/LuaSnip",
                after = "cmp-path"
            }),
            ({
                "hrsh7th/cmp-nvim-lua",
                after = "LuaSnip"
            }),
            ({
                "saadparwaiz1/cmp_luasnip",
                after = "cmp-nvim-lua"
            }),
            ({
                "hrsh7th/cmp-nvim-lsp",
                after = "cmp_luasnip"
            }),
            ({
                "ray-x/cmp-treesitter",
                after = "cmp-nvim-lsp"
            }),
            ({
                "lukas-reineke/cmp-rg",
                after = "cmp-nvim-lsp"
            }),
            ({
                "hrsh7th/cmp-calc",
                after = "cmp-rg"
            }),
            ({
                "hrsh7th/cmp-emoji",
                after = "cmp-calc"
            }),
            ({
                "hrsh7th/cmp-nvim-lsp-signature-help",
                after = "cmp-emoji"
            })
        })
    })
}

require("lazy").setup(plugins, {
    root = root .. "/plugins",
})

-- vim:ft=lua
