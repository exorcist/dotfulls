local g = vim.g

g.netrw_dav_cmd = "curl"
g.netrw_fetch_cmd = ""
g.netrw_ftp_cmd = "ftp"
g.netrw_http_cmd = "curl"
g.netrw_rcp_cmd = "rcp"
g.netrw_rsync_cmd = "rsync"
g.netrw_scp_cmd = "scp -q"
g.netrw_sftp_cmd = "sftp"
g.netrw_ssh_cmd = "ssh"

g.netrw_cygwin = 0
g.netrw_ftp = 1
g.netrw_ftpmode = "binary"
g.netrw_ignorenetrc = 0
g.netrw_sshport = "-p"
g.netrw_silent = 0
g.netrw_use_nt_rcp = 0
g.netrw_win95ftp = 1

g.netrw_use_errorwindow = 1

g.netrw_altfile = 0
g.netrw_alto = 1
g.netrw_altv = 1
g.netrw_banner = 0
g.netrw_browse_split = 0
g.netrw_compress = "gzip"
g.netrw_ctags = "ctags"
g.netrw_cursor = 2
g.netrw_dirhistmax = 10
g.netrw_errorlvl = 0
g.netrw_fastbrowse = 0
g.netrw_fname_escape = " ?&;%"
g.netrw_ftp_list_cmd = "ls -lF"
g.netrw_ftp_sizelist_cmd = "ls -slF"
g.netrw_ftp_timelist_cmd = "ls -tlF"
g.netrw_glob_escape = "*[]?`{~$\""
g.netrw_hide = 1
g.netrw_keepdir = 1
g.netrw_list_cmd = "ssh USEPORT HOSTNAME ls -FLa"
g.netrw_list_hide = ""
g.netrw_liststyle = 3
g.netrw_localcopycmd = "cp"
g.netrw_localcopycmdopt = ""
g.netrw_localmkdir = "mkdir"
g.netrw_localmkdiropt = ""
g.netrw_localmovecmd = "mv"
g.netrw_localmovecmdopt = ""
g.netrw_localrmdiropt = ""
g.netrw_maxfilenamelen = 32
g.netrw_menu = 1
g.netrw_mousemaps = 1
g.netrw_mkdir_cmd = "ssh USEPORT HOSTNAME mkdir"
g.netrw_remote_mkdir = "mkdir"
g.netrw_preview = 1
g.netrw_rename_cmd = "ssh USEPORT HOSTNAME mv"
g.netrw_retmap = 0
g.netrw_rm_cmd = "ssh USEPORT HOSTNAME rm"
g.netrw_rmdir_cmd = "ssh USEPORT HOSTNAME rmdir"
g.netrw_rmf_cmd = "ssh USEPORT HOSTNAME rm -f "
g.netrw_sort_by = "name"
g.netrw_sort_direction = "normal"
g.netrw_sort_options = ""
g.netrw_servername = "NETRWSERVER"
g.netrw_special_syntax = 0
g.netrw_ssh_cmd = "ssh"
g.netrw_scpport = "-P"
g.netrw_sepchr = "ÿ"
g.netrw_sshport = "-p"
g.netrw_timefmt = "%c"
g.netrw_tmpfile_escape = " &;"
g.netrw_use_noswf = 1
g.netrw_xstrlen = 1
g.netrw_winsize = 50
g.netrw_bufsettings = "noma nomod nonu nobl nowrap ro nornu nocursorline nocursorcolumn"
