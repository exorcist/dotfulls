local fn = vim.fn
local api = vim.api
local cmd = vim.cmd
local concat = table.concat
local mod = require("colors").mod

Statusline = ({})

function Statusline.active()
    local mode_map = ({
        ["n"] = "normal",
        ["no"] = "normal operator pending",
        ["v"] = "visual",
        ["V"] = "visual line",
        [""] = "visual block",
        ["s"] = "select",
        ["S"] = "line select",
        [""] = "block select",
        ["i"] = "insert",
        ["R"] = "replace",
        ["Rv"] = "visual replace",
        ["c"] = "command",
        ["cv"] = "vim execute",
        ["ce"] = "execute",
        ["r"] = "prompt",
        ["rm"] = "more",
        ["r?"] = "confirm",
        ["!"] = "shell",
        ["t"] = "terminal"
    })

    local function colorize_mode()
        local function remode (n)
            return(mode_map[n])
        end

        local mode = fn.mode()
        local mode_col = "%#OtherMode#"

        if (mode == "n") then
            mode_col = "%#NormalMode#"
        elseif (mode == "i") then
            mode_col = "%#InsertMode#"
        elseif (mode == "R" or mode == "r" or mode == "Rv") then
            mode_col = "%#ReplaceMode#"
        elseif (mode == "v" or mode == "V") then
            mode_col = "%#VisualMode#"
        elseif (mode == "") then
            mode_col = "%#VisualBlockMode#"
        elseif (mode == "c") then
            mode_col = "%#CommandMode#"
        elseif (mode == "t") then
            mode_col = "%#TerminalMode#"
        end

        return(mode_col .. remode(mode))
    end

    local function filetype()
        local ft = api.nvim_buf_get_option(0, "filetype")

        local ftype

        if (ft == "") then
            ftype = "-"
        else
            ftype = ft
        end

        return("%7* |%5* " .. ftype)
    end

    local line = concat({
        colorize_mode(),
        "%7* |%2* %t",
        mod(),
        "%0*%=",
        "%6*%l",
        filetype(),
        "%0*",
        "%*"
    })

    return(line)
end

function Statusline.inactive()
    return("")
end
