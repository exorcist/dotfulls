local treesitter = require("nvim-treesitter.configs")

treesitter.setup({
    ensure_installed = "all",
    indent = ({
        enable = true
    }),
    highlight = ({
        enable = true,
        use_languagetree = true,
        additional_vim_regex_highlighting = true,
        disable = ({ "sh", "bash", "c_sharp", "fish", "swift" })
    })
})

-- vim:ft=lua
