local g = vim.g
local o = vim.o
local fn = vim.fn
local cmd = vim.cmd
local opt = vim.opt

function _G.join_paths(...)
    return(table.concat(({...}), "/"))
end

g["is_posix"] = 1
g["rehash256"] = 1
g["asmsyntax"] = "fasm"

g["mapleader"] = " "
g["maplocalleader"] = " "

g["gruvbox_bold"] = 1
g["gruvbox_italic"] = 1
g["gruvbox_termcolors"] = 256
g["gruvbox_contrast_dark"] = "hard"

g["&t_Co"] = 256

cmd("syntax on")
cmd("colorscheme gruvbox")
cmd("filetype plugin indent on")

local haskell_opts = ({
    "quantification",
    "recursivedo",
    "arrowsyntax",
    "pattern_synonyms",
    "typeroles",
    "static_pointers"
})

for _, enb in pairs(haskell_opts) do
    g["haskell_enable_" .. enb] = 1
end

g["haskell_backpack"] = 1

g["zig_fmt_autosave"] = 0

local disabled_builtins = ({
    "2html_plugin",
    "getscript",
    "getscriptPlugin",
    "gzip",
    "logipat",
    -- "netrw",
    -- "netrwPlugin",
    -- "netrwSettings",
    -- "netrwFileHandlers",
    "matchit",
    "tar",
    "tarPlugin",
    "rrhelper",
    "spellfile_plugin",
    "vimball",
    "vimballPlugin",
    "zip",
    "zipPlugin"
})

for _, plugin in pairs(disabled_builtins) do
    g["loaded_" .. plugin] = 1
end

opt.nrformats:append("octal")

o.path = ".,**"

o.encoding = "utf-8"
opt.fileencoding = "utf-8"
vim.scriptencoding = "utf-8"

o.switchbuf = "usetab,useopen,uselast"
o.laststatus = 2

o.pumheight = 10

o.virtualedit = "block"

o.lazyredraw = true

opt.matchpairs:append("<:>")
o.formatoptions = "qn1"

o.fileformat = "unix"

o.background = "dark"
o.termguicolors = true

o.compatible = false

o.smarttab = true
o.expandtab = true

o.splitbelow = true
o.splitright = true

o.wrap = true
o.wrapscan = true
o.autoindent = true
o.smartindent = true

o.number = false
o.relativenumber = false

o.cursorline = false
o.cursorcolumn = false

opt.shortmess:append("saAtTIc")

o.linebreak = true
o.breakindent = true

o.tabstop = 4
o.shiftwidth = 4
o.softtabstop = -1

o.incsearch = true
o.smartcase = true
o.ignorecase = true

o.hlsearch = true

opt.fillchars:append({
    eob = " ",
    fold = "*",
    msgsep = "‾",
    horiz = "─",
    horizup = "┴",
    horizdown = "┬",
    vert = "│",
    vertleft = "┤",
    vertright = "├",
    verthoriz = "┼",
})

o.cmdheight = 1

opt.clipboard:append("unnamedplus")

o.title = true
o.titlestring = "%f"

o.autochdir = true

o.magic = true

o.visualbell = false
o.errorbells = false

o.backup = false
o.writebackup = false

o.showcmd = false
o.showmode = false

o.timeout = false
o.ttimeout = false

o.equalalways = false

o.swapfile = false

o.mouse = ""

o.matchtime = 1
o.showmatch = true

o.completeopt = "menu,menuone,noselect,noinsert"

o.scrolloff = 10

o.langremap = true
o.langmap = "АA,аa,БB,бb,ЦC,цc,ДD,дd,ЕE,еe,ФF,фf,ГG,гg,ХH,хh,ИI,иi,ЙJ,йj,КK,кk,ЛL,лl,МM,мm,НN,нn,ОO,оo,ПP,пp,ЯQ,яq,РR,рr,СS,сs,ТT,тt,УU,уu,ЖV,жv,ВW,вw,ѝX,ьx,ЪY,ъy,ЗZ,зz,ч`,11,22,33,44,55,66,77,88,99,00,--,==,ш[,щ],;;,'',ю\\,..,//,Ч~,!!,@@,№#,$$,%%,€^,§&,**,((,)),__,++,Ш{,Щ},::,\"\",Ю|,<<,>>,??"

o.foldnestmax = 3
o.foldenable = true
o.foldmethod = "marker"

o.shell = vim.env.SHELL

o.guicursor = ""

o.ruler = false

o.updatetime = 250

o.signcolumn = "auto"

o.backspace = "indent,eol,start"

o.undofile = true
o.undodir = join_paths(fn.stdpath("data"), "undo")

o.statusline = "%!v:lua.Statusline.active()"

o.list = true
o.listchars = ""
opt.listchars:append({
    tab = "$ ",
    trail = ">",
    extends = "%",
    precedes = "^",
    nbsp = "_"
})

-- vim:ft=lua
