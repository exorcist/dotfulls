local api = vim.api

local function augroup(name, args)
    api.nvim_create_augroup(name, args)
end

local function autocmd(name, args)
    api.nvim_create_autocmd(name, args)
end

augroup("clean", ({ clear = true }))

autocmd("TextYankPost", ({
    group = "clean",
    callback = function()
        vim.highlight.on_yank({
            higroup = "Visual",
            timeout = 120
        })
    end
}))

autocmd("BufWritePost", ({
    group = "clean",
    pattern = ({ "dunstrc", "nw" }),
    command = [[silent !"${HOME:="/home/$USER"}/.local/bin/nw"]]
}))

autocmd("BufWritePost", ({
    group = "clean",
    pattern = ({ "pipewire.conf", "pw" }),
    command = [[silent !"${HOME:="/home/$USER"}/.local/bin/pw"]]
}))

autocmd("BufWritePost", ({
    group = "clean",
    pattern = ({ "resources", "xresources", "xres" }),
    command = [[silent !xrdb -load %]]
}))

autocmd("BufWritePost", ({
    group = "clean",
    pattern = ({ "config.h", "*.c", "*.asm" }),
    command = [[silent !make "$MAKEFLAGS"]]
}))

autocmd("BufWritePost", ({
    group = "clean",
    pattern = "config.def.h",
    command = [[silent !rm config.h && make "$MAKEFLAGS"]]
}))

autocmd("BufWritePost", ({
    group = "clean",
    pattern = "*tmux.conf",
    command = [[silent !tmux source-file %]]
}))

autocmd("BufWritePost", ({
    group = "clean",
    pattern = "remap",
    command = [[silent !%]]
}))

autocmd("TermOpen", ({
    pattern = "*",
    command = "startinsert"
}))

autocmd("BufReadPost", ({
    pattern = "*",
    command = [[if line("'\"") > 1 && line("'\"") <= line("$") && &ft !~# 'commit' | exe "normal! g`\"" | endif]]
}))

api.nvim_create_autocmd("WinEnter", ({
    pattern = "*",
    callback = function()
        vim.opt_local.statusline = "%!v:lua.Statusline.active()"
    end
}))

api.nvim_create_autocmd("WinLeave", ({
    pattern = "*",
    callback = function()
        vim.opt_local.statusline = "%!v:lua.Statusline.inactive()"
    end
}))

autocmd("BufWritePre", ({
    pattern = "*",
    command = [[%s/\s\+$//e | %s/\n\+\%$//e]]
}))

api.nvim_create_autocmd(({ "BufRead", "BufNewFile" }), ({
    pattern = ({ "*.config", "*.conf", "*.cfg", "*.rc", "*.sh" }),
    callback = function()
        vim.opt_local.syntax = "sh"
    end
}))

api.nvim_create_autocmd(({ "BufRead", "BufNewFile" }), ({
    pattern = ({ "*.hs", "*.hsc" }),
    callback = function()
        vim.opt_local.syntax = "haskell"
    end
}))

api.nvim_create_autocmd(({ "BufRead", "BufNewFile" }), ({
    pattern = "*.ms",
    callback = function()
        vim.opt_local.syntax = "groff"
    end
}))

api.nvim_create_autocmd(({ "BufRead", "BufNewFile" }), ({
    pattern = "*.pad",
    callback = function()
        vim.opt_local.syntax = "markdown"
    end
}))

api.nvim_create_autocmd(({ "BufRead", "BufNewFile" }), ({
    pattern = "*.txt",
    callback = function()
        vim.opt_local.syntax = "text"
    end
}))

api.nvim_create_autocmd(({ "BufRead", "BufNewFile" }), ({
    pattern = "*.pl",
    callback = function()
        vim.opt_local.syntax = "perl"
    end
}))

api.nvim_create_autocmd(({ "BufRead", "BufNewFile" }), ({
    pattern = "*.patch",
    callback = function()
        vim.opt.syntax = "gitsendemail"
    end
}))

api.nvim_create_autocmd(({ "BufRead", "BufNewFile" }), ({
    pattern = "*.asm",
    callback = function()
        vim.opt_local.syntax = "fasm"
    end
}))

autocmd("Filetype", ({
    pattern = ({ "tex", "html", "text", "markdown" }),
    callback = function()
        vim.opt_local.spelllang = "en_us,bg"
    end
}))

api.nvim_create_autocmd(({ "CursorHold", "CursorHoldI" }), ({
    pattern = "*",
    callback = function()
        vim.diagnostic.open_float(nil, ({ focus = false }))
    end
}))

-- autocmd("BufWritePre", ({
--     pattern = "plugins.lua",
--     command = [[source % | PackerSync]]
-- }))

-- vim:ft=lua
