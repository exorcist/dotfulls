local fn = vim.fn
local api = vim.api
local cmp = require("cmp")
local cmp_window = require("cmp.utils.window")
local _, luasnip = pcall(require, "luasnip")

cmp_window.info_ = cmp_window.info
cmp_window.info = function(self)
    local info = self:info_()

    info.scrollable = false

    return(info)
end

local icons = ({
    Text = "[text]",
    Method = "[method]",
    Function = "[fn]",
    Constructor = "[cons]",
    Field = "[field]",
    Variable = "[var]",
    Class = "[class]",
    Interface = "[int]",
    Module = "[mod]",
    Property = "[prop]",
    Unit = "[unit]",
    Value = "[val]",
    Enum = "[enum]",
    Keyword = "[keyword]",
    Snippet = "[snip]",
    Color = "[color]",
    File = "[file]",
    Reference = "[link]",
    Folder = "[folder]",
    EnumMember = "[enum mem]",
    Constant = "[const]",
    Struct = "[struct]",
    Event = "[event]",
    Operator = "[op]",
    TypeParameter = "[tp]"
})

local border = ({ "┌", "─", "┐", "│", "┘", "─", "└", "│" })

local mappings = cmp.mapping.preset.insert({
        ["<C-p>"] = cmp.mapping.select_prev_item(),
        ["<C-n>"] = cmp.mapping.select_next_item(),
        ["<C-d>"] = cmp.mapping.scroll_docs(-4),
        ["<C-f>"] = cmp.mapping.scroll_docs(4),
        ["<C-Space>"] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping.close(),
        ["<CR>"] = cmp.mapping.confirm({
            select = false
        }),
        ["<Tab>"] = function(fallback)
            if (cmp.visible()) then
                cmp.select_next_item()
            elseif (luasnip.expand_or_jumpable()) then
                fn.feedkeys(api.nvim_replace_termcodes("<Plug>luasnip-expand-or-jump", true, true, true), "")
            else
                fallback()
            end
        end,
        ["<S-Tab>"] = function(fallback)
            if (cmp.visible()) then
                cmp.select_prev_item()
            elseif (luasnip.jumpable(-1)) then
                fn.feedkeys(api.nvim_replace_termcodes("<Plug>luasnip-jump-prev", true, true, true), "")
            else
                fallback()
            end
        end
    })

local highlight = "Normal:Normal,FloatBorder:Normal,CursorLine:PmenuSel,Search:Normal"

cmp.setup({
    snippet = ({
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end
    }),
    experimental = ({
        ghost_text = false,
        native_menu = false
    }),
    view = ({
        entries = ({
            name = "custom",
            selection_order = "near_cursor"
        })
    }),
    mapping = mappings,
    formatting = ({
        fields = ({ "kind", "abbr", "menu" }),
        format = function(_, vim_item)
            vim_item.kind = icons[vim_item.kind]
            return(vim_item)
        end
    }),
    window = ({
        documentation = ({
            border = border,
            winhighlight = highlight
        }),
        completion = ({
            border = border,
            winhighlight = highlight
        })
    }),
    sources = ({
        ({ name = "rg", priority = 4 }),
        ({ name = "calc", priority = 4 }),
        ({ name = "path", priority = 1 }),
        ({ name = "emoji", priority = 3 }),
        ({ name = "spell", priority = 2 }),
        ({ name = "luasnip", priority = 3 }),
        ({ name = "nvim_lua", priority = 1 }),
        ({ name = "nvim_lsp", priority = 10 }),
        ({ name = "treesitter", priority = 4 }),
        ({ name = "nvim_lsp_signature_help", priority = 10 }),
        ({ name = "buffer", keyword_length = 2, priority = 5 })
    }),
    preselect = cmp.PreselectMode.None
})

-- vim:ft=lua
