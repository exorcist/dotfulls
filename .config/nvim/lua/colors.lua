local M = ({})
local api = vim.api

M = ({
    colors = ({
        basebg = "#1d2021",
        basefg = "#ebdbb2",
        base00 = "#1d2021",
        base08 = "#ebdbb2",
        base01 = "#cc241d",
        base09 = "#fb4934",
        base02 = "#98971a",
        base10 = "#b8bb26",
        base03 = "#d65d0e",
        base11 = "#d65d0e",
        base04 = "#458588",
        base12 = "#83a598",
        base05 = "#b16286",
        base13 = "#d3869b",
        base06 = "#689d6a",
        base14 = "#8ec07c",
        base07 = "#a89984",
        base15 = "#ebdbb2",
        base16 = "#d65d0e",
        base17 = "#282828",
        base18 = "#32302f"
    })
})

function M.hi(group, args)
    api.nvim_set_hl(0, group, args or ({}))
end

function M.mod()
    local modified = api.nvim_buf_get_option(0, "modified")

    local state

    if (modified == false) then
        M.hi("Mod", ({ bg = M.colors.basebg, fg = M.colors.basebg }))
        state = ""
    else
        M.hi("Mod", ({ bg = M.colors.basebg, fg = M.colors.base03 }))
        state = "%7* |%#Mod# *"
    end

    return("%0*" .. state)
end

M.hi("NormalFloat", ({ link = "Normal" }))
M.hi("FloatBorder", ({ link = "Normal" }))

M.hi("CursorLine", ({ link = "PmenuSel" }))

M.hi("Pmenu", ({ bg = M.colors.base17, fg = M.colors.basefg }))
M.hi("PmenuSel", ({ bg = M.colors.base12, fg = M.colors.basebg }))
M.hi("PmenuSbar", ({ bg = M.colors.base18, fg = M.colors.basebg }))
M.hi("PmenuThumb", ({ bg = M.colors.base06, fg = M.colors.basebg }))

M.hi("WildMenu")

M.hi("CmpItemSel", ({ bg = M.colors.base12, fg = M.colors.basebg }))
M.hi("CmpItemMenu", ({ bg = M.colors.base12, fg = M.colors.basebg, bold = true }))
M.hi("CmpItemAbbr", ({ bg = M.colors.basebg, fg = M.colors.basefg }))
M.hi("CmpItemKind", ({ bg = M.colors.basebg, fg = M.colors.base11 }))
M.hi("CmpItemAbbrMatch", ({ bg = M.colors.base12, fg = M.colors.basebg }))
M.hi("CmpItemAbbrMatchFuzzy", ({ bg = M.colors.base12, fg = M.colors.basebg }))
M.hi("CmpItemAbbrDeprecated", ({ bg = M.colors.basebg, fg = M.colors.basefg }))

M.hi("LineNr", ({ bg = M.colors.basebg, fg = M.colors.base05, bold = true }))
M.hi("LineNrAbove", ({ bg = M.colors.basebg, fg = M.colors.base18 }))
M.hi("LineNrBelow", ({ bg = M.colors.basebg, fg = M.colors.base18 }))
M.hi("SignColumn", ({ bg = M.colors.basebg, fg = M.colors.base18 }))
M.hi("CursorLineNr", ({ bg = M.colors.basebg, fg = M.colors.base05, bold = true }))

M.hi("Todo", ({ bg = M.colors.basebg, fg = M.colors.base16, italic = true }))
M.hi("Comment", ({ bg = M.colors.basebg, fg = M.colors.base18, italic = true }))

M.hi("TabLine", ({ bg = M.colors.basebg, fg = M.colors.base18 }))
M.hi("TabLineSel", ({ bg = M.colors.basebg, fg = M.colors.base10, bold = true }))
M.hi("TabLineFill", ({ bg = M.colors.basebg, fg = M.colors.basebg }))

M.hi("StatusLine", ({ bg = M.colors.basebg, fg = M.colors.basebg }))
M.hi("StatusLineNC", ({ bg = M.colors.basebg, fg = M.colors.basebg }))

M.hi("Error", ({ bg = M.colors.basebg, fg = M.colors.base01, bold = true }))
M.hi("Folded", ({ bg = M.colors.basebg, fg = M.colors.base08, italic = true }))
M.hi("VertSplit", ({ bg = M.colors.basebg, fg = M.colors.base18, bold = true }))

M.hi("DiagnosticSignHint", ({ bg = M.colors.basebg, fg = M.colors.base06 }))
M.hi("DiagnosticSignWarn", ({ bg = M.colors.basebg, fg = M.colors.base03 }))
M.hi("DiagnosticSignInfo", ({ bg = M.colors.basebg, fg = M.colors.base12 }))
M.hi("DiagnosticSignError", ({ bg = M.colors.basebg, fg = M.colors.base01 }))

M.hi("User0", ({ bg = M.colors.basebg, fg = M.colors.basebg }))
M.hi("User1", ({ bg = M.colors.basebg, fg = M.colors.basefg }))
M.hi("User2", ({ bg = M.colors.basebg, fg = M.colors.base10, bold = true }))
M.hi("User4", ({ bg = M.colors.basebg, fg = M.colors.base04 }))
M.hi("User5", ({ bg = M.colors.basebg, fg = M.colors.base01 }))
M.hi("User6", ({ bg = M.colors.basebg, fg = M.colors.base05, italic = true }))
M.hi("User7", ({ bg = M.colors.basebg, fg = M.colors.base18 }))
M.hi("User9", ({ bg = M.colors.basebg, fg = M.colors.base06 }))

M.hi("Branch", ({ bg = M.colors.basebg, fg = M.colors.base12 }))
M.hi("OtherMode", ({ bg = M.colors.basebg, fg = M.colors.base12 }))
M.hi("VisualMode", ({ bg = M.colors.basebg, fg = M.colors.base02 }))
M.hi("NormalMode", ({ bg = M.colors.basebg, fg = M.colors.base04 }))
M.hi("InsertMode", ({ bg = M.colors.basebg, fg = M.colors.base06, bold = true }))
M.hi("ReplaceMode", ({ bg = M.colors.basebg, fg = M.colors.base05, bold = true }))
M.hi("CommandMode", ({ bg = M.colors.basebg, fg = M.colors.base16 }))
M.hi("TerminalMode", ({ bg = M.colors.basebg, fg = M.colors.base01 }))
M.hi("VisualBlockMode", ({ bg = M.colors.basebg, fg = M.colors.base03 }))

return(M)

-- vim:ft=lua
