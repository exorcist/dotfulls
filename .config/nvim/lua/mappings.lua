local fn = vim.fn
local fmt = string.format
local map = vim.api.nvim_set_keymap
local opts = ({ noremap = true, silent = true })

local function nnoremap(k, c, o)
    map("n", k, c, o)
end

local function inoremap(k, c, o)
    map("i", k, c, o)
end

local function vnoremap(k, c, o)
    map("v", k, c, o)
end

local function cnoremap(k, c, o)
    map("c", k, c, o)
end

map("n", ";", ":", ({ noremap = true }))

nnoremap("<F1>", "<nop>", opts)
nnoremap("<A-S-j>", "<cmd>resize -5<cr>", opts)
nnoremap("<A-S-k>", "<cmd>resize +5<cr>", opts)
nnoremap("<A-S-h>", "<cmd>vertical resize +5<cr>", opts)
nnoremap("<A-S-l>", "<cmd>vertical resize -5<cr>", opts)
nnoremap("<leader>cf", fmt("<cmd>luafile %s<cr>", fn.stdpath("config") .. "/init.lua"), ({ noremap = true, silent = false }))
nnoremap("<leader>bc", fmt("<cmd>Explore %s<cr>", fn.stdpath("config") .. "/lua"), opts)
nnoremap("<leader>tt", "<cmd>split | resize -10 | term<cr>", opts)
nnoremap("<leader>ee", "<cmd>Explore .<cr>", opts)
nnoremap("<leader>ff", "<cmd>Explore .<cr>", opts)
nnoremap("<leader>h", "<cmd>wincmd h<cr>", opts)
nnoremap("<leader>j", "<cmd>wincmd j<cr>", opts)
nnoremap("<leader>k", "<cmd>wincmd k<cr>", opts)
nnoremap("<leader>l", "<cmd>wincmd l<cr>", opts)
nnoremap("<leader>v", "<cmd>vertical split<cr>", opts)
nnoremap("<leader>s", "<cmd>split<cr>", opts)
nnoremap("<leader>o", "o<Esc>O", opts)
nnoremap("r", "R", opts)
nnoremap("j", "gj", opts)
nnoremap("k", "gk", opts)
nnoremap("H", "g0", opts)
nnoremap("L", "g$", opts)
nnoremap("Q", "gwip", opts)
nnoremap("U", "<C-r>", opts)

inoremap("<F1>", "<nop>", opts)
inoremap("<C-q>", "<Esc>gwip i", opts)
inoremap("<A-x>", "<C-g>u<Esc>[s1z=`]a<C-g>u", opts)

nnoremap("<leader>pu", "<cmd>PackerSync<cr>", opts)
nnoremap("<leader>pc", "<cmd>PackerClean<cr>", opts)
nnoremap("<leader>pg", "<cmd>PackerUpdate<cr>", opts)

nnoremap("<leader>pt", "<cmd>tabprev<cr>", opts)
nnoremap("<leader>nt", "<cmd>tabnext<cr>", opts)
nnoremap("<leader>tn", "<cmd>tabnew<cr>:Explore .<cr>", opts)

vnoremap("<leader>p", "\"_dP", opts)

nnoremap("<C-q>", "ZZ", opts)
nnoremap("<C-s>", "<cmd>w<cr>", opts)
nnoremap("<C-c>", "<C-w>c<cr>", opts)
nnoremap("<C-w>", "<cmd>set cursorline!<cr>", opts)
nnoremap("<C-x>", "<cmd>set nonumber! norelativenumber!<cr>", opts)

nnoremap("<leader>nn", "<cmd>next<cr>", opts)
nnoremap("<leader>pp", "<cmd>prev<cr>", opts)

nnoremap("n", "nzzzv", opts)
nnoremap("N", "Nzzzv", opts)
nnoremap("J", "mzJ`z", opts)

nnoremap("<S-up>", "<nop>", opts)
nnoremap("<S-down>", "<nop>", opts)
nnoremap("<S-left>", "<nop>", opts)
nnoremap("<S-right>", "<nop>", opts)
nnoremap("<C-up>", "<nop>", opts)
nnoremap("<C-down>", "<nop>", opts)
nnoremap("<C-left>", "<nop>", opts)
nnoremap("<C-right>", "<nop>", opts)
nnoremap("<up>", "<nop>", opts)
nnoremap("<down>", "<nop>", opts)
nnoremap("<left>", "<nop>", opts)
nnoremap("<right>", "<nop>", opts)

nnoremap("<leader>de", "!!date -I<cr>", opts)

inoremap("<S-up>", "<nop>", opts)
inoremap("<S-down>", "<nop>", opts)
inoremap("<S-left>", "<nop>", opts)
inoremap("<S-right>", "<nop>", opts)
inoremap("<C-up>", "<nop>", opts)
inoremap("<C-down>", "<nop>", opts)
inoremap("<C-left>", "<nop>", opts)
inoremap("<C-right>", "<nop>", opts)
inoremap("<up>", "<nop>", opts)
inoremap("<down>", "<nop>", opts)
inoremap("<left>", "<nop>", opts)
inoremap("<right>", "<nop>", opts)

vnoremap("<S-up>", "<nop>", opts)
vnoremap("<S-down>", "<nop>", opts)
vnoremap("<S-left>", "<nop>", opts)
vnoremap("<S-right>", "<nop>", opts)
vnoremap("<C-up>", "<nop>", opts)
vnoremap("<C-down>", "<nop>", opts)
vnoremap("<C-left>", "<nop>", opts)
vnoremap("<C-right>", "<nop>", opts)
vnoremap("<up>", "<nop>", opts)
vnoremap("<down>", "<nop>", opts)
vnoremap("<left>", "<nop>", opts)
vnoremap("<right>", "<nop>", opts)

cnoremap("w!!", "w !ssu tee % >'/dev/null'", opts)

nnoremap ("<A-j>", "<cmd>m .+1<cr>==", opts)
nnoremap ("<A-k>", "<cmd>m .-2<cr>==", opts)
inoremap ("<A-j>", "<Esc><cmd>m .+1<cr>==gi", opts)
inoremap ("<A-k>", "<Esc><cmd>m .-2<cr>==gi", opts)
vnoremap ("<A-j>", "<cmd>m '>+1<cr>gv=gv", opts)
vnoremap ("<A-k>", "<cmd>m '<-2<cr>gv=gv", opts)

-- vim:ft=lua
