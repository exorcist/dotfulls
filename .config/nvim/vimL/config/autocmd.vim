autocmd BufWritePost dunstrc,nw silent ! "${HOME:-"/home/$USER"}/.local/bin/nw"
autocmd BufWritePost Xresources,xres,xresources silent ! xrdb -load %
autocmd BufWritePost config.h,*.c silent ! make "$MAKEFLAGS"
autocmd BufWritePost config.def.h silent ! rm config.h && make "$MAKEFLAGS"
autocmd BufWritePost *tmux.conf silent ! tmux source-file %
autocmd BufWritePost remap silent ! %

autocmd CursorHold,CursorHoldI * lua vim.lsp.diagnostic.show_line_diagnostics({focusable=false})

autocmd TermOpen * startinsert

autocmd BufReadPost *
            \ if line("'\"") > 1 && line("'\"") <= line("$") && &ft !~# 'commit'
            \ |   exe "normal! g`\""
            \ | endif

autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\n\+\%$//e
autocmd BufNewFile,BufRead *.config setlocal syntax=sh
autocmd BufNewFile,BufRead *.conf setlocal syntax=sh
autocmd BufNewFile,BufRead *.cfg setlocal syntax=sh
autocmd BufNewFile,BufRead *.rc setlocal syntax=sh
autocmd BufNewFile,BufRead *.sh setlocal syntax=sh
autocmd BufNewFile,BufRead *.patch setlocal syntax=diff
autocmd BufNewFile,BufRead *.hs,*.hsc setlocal syntax=haskell
autocmd BufNewFile,BufRead *.hs,*.hsc setlocal ft=haskell
autocmd BufNewFile,BufRead *.pl setlocal syntax=perl
autocmd BufNewFile,BufRead *.txt setlocal syntax=text
autocmd BufNewFile,BufRead *.pad setlocal syntax=md
autocmd BufNewFile,BufRead *.ms setlocal syntax=groff
autocmd BufNewFile,BufRead *.asm setlocal ft=nasm

autocmd FileType rmd map <F5> :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter>

autocmd Filetype tex setlocal spell spelllang=en_us
autocmd Filetype html setlocal spell spelllang=en_us
autocmd Filetype text setlocal spell spelllang=en_us
autocmd FileType markdown setlocal spell spelllang=en_us

augroup StatusLine
    autocmd WinEnter * setlocal statusline=%!StatusLine('active')
    autocmd WinLeave * setlocal statusline=%!StatusLine('inactive')
augroup END

augroup UnLoad
    autocmd BufRead * let g:loaded_gzip = 0
    autocmd BufRead * let g:loaded_tar = 0
    autocmd BufRead * let g:loaded_tarPlugin = 0
    autocmd BufRead * let g:loaded_zipPlugin = 0
    autocmd BufRead * let g:loaded_2html_plugin = 0
    autocmd BufRead * let g:loaded_spec = 0
    autocmd BufRead * let g:loaded_syncolor = 0
augroup END

" vim:ft=vim
