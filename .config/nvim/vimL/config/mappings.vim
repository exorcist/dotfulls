nnoremap <silent><leader>h :wincmd h<cr>
nnoremap <silent><leader>j :wincmd j<cr>
nnoremap <silent><leader>k :wincmd k<cr>
nnoremap <silent><leader>l :wincmd l<cr>

nnoremap а a
nnoremap А A
nnoremap И I
nnoremap и i
nnoremap в v
nnoremap В V
nnoremap у u
nnoremap У <C-r>
nnoremap й gj
nnoremap к gk
nnoremap х h
nnoremap л l
nnoremap <C-в> <C-v>

nnoremap <silent><A-S-j> :resize -5<cr>
nnoremap <silent><A-S-k> :resize +5<cr>
nnoremap <silent><A-h> :vertical resize -5<cr>
nnoremap <silent><A-l> :vertical resize +5<cr>

nnoremap <silent><leader>ss :so $MYVIMRC<cr>
nnoremap <silent><leader>so :edit $XDG_CONFIG_HOME/nvim/vimL/config<cr>
nnoremap <silent><leader>tt :term<cr>
nnoremap <silent><leader>ee :Explore<cr>
nnoremap <silent><leader>ff :Files<cr>
nnoremap <silent><leader>dd :Buffers<cr>

nnoremap ; :
nnoremap r R
nnoremap j gj
nnoremap k gk
nnoremap H g0
nnoremap L g$
nnoremap Q gwip
nnoremap U <C-r>

inoremap <C-q> <Esc>gwip i
inoremap <A-l> <c-g>u<Esc>[s1z=`]a<c-g>u

nnoremap <silent>gd <cmd>lua vim.lsp.buf.definition()<cr>
nnoremap <silent>gD <cmd>lua vim.lsp.buf.declaration()<cr>
nnoremap <silent>gr <cmd>lua vim.lsp.buf.references()<cr>
nnoremap <silent>gi <cmd>lua vim.lsp.buf.implementation()<cr>
nnoremap <silent>K <cmd>lua vim.lsp.buf.hover()<cr>
nnoremap <silent><C-k> <cmd>lua vim.lsp.buf.signature_help()<cr>
nnoremap <silent><C-p> <cmd>lua vim.lsp.diagnostic.goto_prev()<cr>
nnoremap <silent><C-n> <cmd>lua vim.lsp.diagnostic.goto_next()<cr>

nnoremap <silent><leader>pu :PlugUpdate<cr>
nnoremap <silent><leader>pi :PlugInstall<cr>
nnoremap <silent><leader>pc :PlugClean<cr>
nnoremap <silent><leader>pg :PlugUpgrade<cr>

nnoremap <silent><leader>pt :tabprev<cr>
nnoremap <silent><leader>nt :tabnext<cr>
nnoremap <silent><leader>tn :tabnew<cr>:Explore . <cr>

nnoremap <silent><C-q> ZZ<cr>
nnoremap <silent><C-c> <C-w>c<cr>
nnoremap <silent><C-a> :set cursorline!<cr>
nnoremap <silent><C-x> :set nonumber! norelativenumber!<cr>

nnoremap <silent><leader>nn :next<cr>
nnoremap <silent><leader>pp :prev<cr>

inoremap <S-up> <nop>
inoremap <S-right> <nop>
inoremap <S-down> <nop>
inoremap <S-left> <nop>
inoremap <up> <nop>
inoremap <right> <nop>
inoremap <down> <nop>
inoremap <left> <nop>

vnoremap <S-up> <nop>
vnoremap <S-right> <nop>
vnoremap <S-down> <nop>
vnoremap <S-left> <nop>
vnoremap <up> <nop>
vnoremap <right> <nop>
vnoremap <down> <nop>
vnoremap <left> <nop>

nnoremap <S-up> <nop>
nnoremap <S-right> <nop>
nnoremap <S-down> <nop>
nnoremap <S-left> <nop>
nnoremap <up> <nop>
nnoremap <right> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>

cnoremap в w
cnoremap В w
cnoremap вя wq
cnoremap я q
cnoremap Я q
cnoremap ВЯ wq
cnoremap в!! w !ssu tee % >"/dev/null"
cnoremap вя! wq!
cnoremap я! q!
cnoremap в! w!
cnoremap Вя wq

cnoremap w!! w !ssu tee % >"/dev/null"

nmap <silent><A-j> :m .+1<cr>==
nmap <silent><A-k> :m .-2<cr>==
imap <silent><A-j> <Esc>:m .+1<cr>==gi
imap <silent><A-k> <Esc>:m .-2<cr>==gi
vmap <silent><A-j> :m '>+1<cr>gv=gv
vmap <silent><A-k> :m '<-2<cr>gv=gv

" cnoremap WQ wq
" cnoremap Wq wq
" cnoremap wQ wq

" vim:ft=vim
