iabbrev adn and
iabbrev taht that
iabbrev tehn then
iabbrev hte the
iabbrev teh the
iabbrev hwo how
iabbrev wehn when
iabbrev tehre there
iabbrev tihs this
iabbrev thsi this
iabbrev shti this
iabbrev THat That
iabbrev THe The
iabbrev abotu about
iabbrev acn can
iabbrev adn and
iabbrev aer are
iabbrev agian again
iabbrev ahev have
iabbrev ahve have
iabbrev alos also
iabbrev alse else
iabbrev alsot also
iabbrev amde made
iabbrev amke make
iabbrev amkes makes
iabbrev anbd and
iabbrev andd and
iabbrev anf and
iabbrev ans and
iabbrev aobut about
iabbrev aslo also
iabbrev awya  away
iabbrev aywa away
iabbrev bakc back
iabbrev baout about
iabbrev bcak back
iabbrev beacuse because
iabbrev becuase because
iabbrev bve be
iabbrev chaneg change
iabbrev chanegs changes
iabbrev chekc check
iabbrev chnage change
iabbrev chnaged changed
iabbrev chnages changes
iabbrev claer clear
iabbrev cmo com
iabbrev cna can
iabbrev coudl could
iabbrev cpoy copy
iabbrev dael deal
iabbrev diea idea
iabbrev doens does
iabbrev doese does
iabbrev doign doing
iabbrev doimg doing
iabbrev donig doing
iabbrev dont don't
iabbrev eahc each
iabbrev efel feel
iabbrev ehlp help
iabbrev ehr her
iabbrev efw few
iabbrev emial email
iabbrev ened need
iabbrev enxt next
iabbrev esle else
iabbrev ew we
iabbrev eyar year
iabbrev eyt yet
iabbrev fatc fact
iabbrev fidn find
iabbrev fiel file
iabbrev firts first
iabbrev flase false
iabbrev fo of
iabbrev fomr form
iabbrev fora for a
iabbrev foudn found
iabbrev frmo from
iabbrev fro for
iabbrev frome from
iabbrev fromthe from the
iabbrev fwe few
iabbrev gerat great
iabbrev gievn given
iabbrev goign going
iabbrev gonig going
iabbrev gruop group
iabbrev grwo grow
iabbrev haev have
iabbrev hasa has a
iabbrev havea have a
iabbrev hda had
iabbrev hge he
iabbrev hlep help
iabbrev holf hold
iabbrev hsa has
iabbrev hsi his
iabbrev htan than
iabbrev htat that
iabbrev hte the
iabbrev htem them
iabbrev hten then
iabbrev htere there
iabbrev htese these
iabbrev htey they
iabbrev hting thing
iabbrev htink think
iabbrev htis this
iabbrev hvae have
iabbrev hvaing having
iabbrev hvea have
iabbrev hwich which
iabbrev hwo how
iabbrev idae idea
iabbrev idaes ideas
iabbrev ihs his
iabbrev inpoet import
iabbrev inport import
iabbrev impoet import
iabbrev impoert import
iabbrev impoer import
iabbrev ina in a
iabbrev inot into
iabbrev inteh in the
iabbrev inthe in the
iabbrev inthese in these
iabbrev inthis in this
iabbrev inwhich in which
iabbrev isthe is the
iabbrev isze size
iabbrev itis it is
iabbrev itwas it was
iabbrev iused used
iabbrev iwll will
iabbrev iwth with
iabbrev jstu just
iabbrev jsut just
iabbrev knwo know
iabbrev knwon known
iabbrev knwos knows
iabbrev konw know
iabbrev konwn known
iabbrev konws knows
iabbrev kwno know
iabbrev laod load
iabbrev lastr last
iabbrev layed laid
iabbrev liek like
iabbrev liekd liked
iabbrev liev live
iabbrev likly likely
iabbrev ling long
iabbrev liuke like
iabbrev loev love
iabbrev lsat last
iabbrev lveo love
iabbrev lvoe love
iabbrev mcuh much
iabbrev mear mere
iabbrev mial mail
iabbrev mkae make
iabbrev mkaes makes
iabbrev mkea make
iabbrev moeny money
iabbrev mroe more
iabbrev msut must
iabbrev muhc much
iabbrev muts must
iabbrev mysefl myself
iabbrev myu my
iabbrev nad and
iabbrev niether neither
iabbrev nkow know
iabbrev nkwo know
iabbrev nmae name
iabbrev nowe now
iabbrev nto not
iabbrev nver never
iabbrev nwe new
iabbrev nwo now
iabbrev ocur occur
iabbrev ofa of a
iabbrev ofits of its
iabbrev ofthe of the
iabbrev oging going
iabbrev ohter other
iabbrev omre more
iabbrev oneof one of
iabbrev onthe on the
iabbrev onyl only
iabbrev ot to
iabbrev otehr other
iabbrev otu out
iabbrev outof out of
iabbrev owrk work
iabbrev owuld would
iabbrev paide paid
iabbrev peice piece
iabbrev puhs push
iabbrev pwoer power
iabbrev rela real
iabbrev rulle rule
iabbrev rwite write
iabbrev sasy says
iabbrev seh she
iabbrev shoudl should
iabbrev sitll still
iabbrev sleect select
iabbrev smae same
iabbrev smoe some
iabbrev sned send
iabbrev soem some
iabbrev sohw show
iabbrev soze size
iabbrev stnad stand
iabbrev stpo stop
iabbrev syas says
iabbrev ta at
iabbrev tahn than
iabbrev taht that
iabbrev tath that
iabbrev teh the
iabbrev tehir their
iabbrev tehn then
iabbrev tehre there
iabbrev tehy they
iabbrev tghe the
iabbrev tghis this
iabbrev thansk thanks
iabbrev thast that
iabbrev thats that's
iabbrev theh then
iabbrev theri their
iabbrev thgat that
iabbrev thge the
iabbrev thier their
iabbrev thign thing
iabbrev thme them
iabbrev thn then
iabbrev thna than
iabbrev thne then
iabbrev thnig thing
iabbrev thre there
iabbrev thsi this
iabbrev thsoe those
iabbrev thta that
iabbrev thyat that
iabbrev thye they
iabbrev ti it
iabbrev tiem time
iabbrev tihs this
iabbrev timne time
iabbrev tiome time
iabbrev tje the
iabbrev tjhe the
iabbrev tkae take
iabbrev tkaes takes
iabbrev tkaing taking
iabbrev todya today
iabbrev tothe to the
iabbrev towrad toward
iabbrev tthe the
iabbrev ture true
iabbrev twpo two
iabbrev tyhat that
iabbrev tyhe the
iabbrev tyhe they
iabbrev uise use
iabbrev untill until
iabbrev veyr very
iabbrev vrey very
iabbrev waht what
iabbrev wass was
iabbrev watn want
iabbrev weas was
iabbrev wehn when
iabbrev werre were
iabbrev whcih which
iabbrev wherre where
iabbrev whic which
iabbrev whihc which
iabbrev whn when
iabbrev whta what
iabbrev wih with
iabbrev wihch which
iabbrev wiht with
iabbrev willk will
iabbrev withe with
iabbrev withh with
iabbrev witht with
iabbrev witn with
iabbrev wiull will
iabbrev wnat want
iabbrev wnats wants
iabbrev woh who
iabbrev wohle whole
iabbrev wokr work
iabbrev woudl would
iabbrev wrod word
iabbrev wroet wrote
iabbrev wrok work
iabbrev wtih with
iabbrev wuould would
iabbrev wya way
iabbrev isnt is not
iabbrev theres there is
iabbrev xmoand xmonad
iabbrev XMoand XMonad
iabbrev yaer year
iabbrev yera year
iabbrev yoiu you
iabbrev yoru your
iabbrev yrea year
iabbrev ytou you
iabbrev yuo you
iabbrev yuor your
iabbrev didnot did not
iabbrev Theyare They are
iabbrev theyare they are
iabbrev aboutit about it
iabbrev thatthe that the
iabbrev alot a lot
iabbrev withthe with the
iabbrev withit with it
iabbrev willbe will be
iabbrev witha with a
iabbrev thanit than it
iabbrev asthe as the
iabbrev atthe at the
iabbrev forthe for the
iabbrev ive I have
iabbrev Ive I have
iabbrev im I am
iabbrev Im I am
iabbrev cant can not
iabbrev didnt did not
iabbrev doesnt does not
iabbrev theyre they are
iabbrev wouldnt would not
iabbrev youre you are
iabbrev youve you have
iabbrev youare you are
iabbrev Youre You are

" vim:ft=vim
