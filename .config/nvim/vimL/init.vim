" This config is no longer supported, since I have made the sensible switch to a lua config

source $XDG_CONFIG_HOME/nvim/vimL/config/settings.vim
source $XDG_CONFIG_HOME/nvim/vimL/config/plugins.vim

if has("nvim")
    source $XDG_CONFIG_HOME/nvim/vimL/config/lua.vim
endif

source $XDG_CONFIG_HOME/nvim/vimL/config/autocmd.vim
source $XDG_CONFIG_HOME/nvim/vimL/config/corrections.vim
source $XDG_CONFIG_HOME/nvim/vimL/config/fm.vim
source $XDG_CONFIG_HOME/nvim/vimL/config/mappings.vim
source $XDG_CONFIG_HOME/nvim/vimL/config/colors.vim

" vim:ft=vim
