local basebg = "#1d2021"
local basefg = "#ebdbb2"

return ({
    color_background = basebg,
    color_foreground = basefg,
    color_bold = basefg,
    color_cursor = basefg,
    color_cursor_foreground = basebg,
    color_highlight = basefg,
    color_highlight_foreground = basebg,
    color_0  = basebg,
    color_1  = "#cc241d",
    color_2  = "#98971a",
    color_3  = "#d79921",
    color_4  = "#458588",
    color_5  = "#b16286",
    color_6  = "#689d6a",
    color_7  = basefg,
    color_8  = "#928374",
    color_9  = "#fb4934",
    color_10 = "#b8bb26",
    color_11 = "#fabd2f",
    color_12 = "#83a598",
    color_13 = "#d3869b",
    color_14 = "#8ec07c",
    color_15 = "#a89984",
    color_window_background = basebg
})
