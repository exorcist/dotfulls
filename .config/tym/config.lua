local tym = require("tym")

tym.set_config({
    title = "tym",

    font = "AnkaCoder Condensed 14px",
    cell_height = 100,
    cell_width = 90,
    bold_is_bright = true,

    autohide = true,
    cursor_shape = "block",
    cursor_blink_mode = "off",
    cjk_width = "narrow",
    term = "xterm-256color",

    padding_top = 15,
    padding_bottom = 15,
    padding_left = 15,
    padding_right = 15,

    scrollback_length = 1024,
})
