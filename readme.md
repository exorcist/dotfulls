## Themes
### Gruvbox
#### Dark
![img](.config/scrots/scrot.png "Dark Mode")
#### Light
![img](.config/scrots/scrot-light.png "Light Mode")

* [fonts](https://codeberg.org/exorcist/fonts)
* [wallpapers](https://codeberg.org/exorcist/wallpapers)

[#]: <> (vim:ft=markdown)
