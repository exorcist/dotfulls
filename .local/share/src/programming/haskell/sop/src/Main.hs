import Prelude ( ($)
               , (.)
               , (-)
               , (>=)
               , (/)
               , IO ()
               , Maybe (Just)
               , Either (Right)
               , fst
               , snd
               , pure
               )

import Control.Monad (void)

import Network.MPD ( Status ( Status
                            , stTime
                            , stSongPos
                            )
                   , play
                   , status
                   , withMPD
                   )

main :: IO ()
main = do
    stat <- withMPD status

    case stat of
      Right ( Status { stTime = Just x
                     , stSongPos = Just y
                     }
            ) -> if fst x >= snd x/2
                    then void . withMPD $ play (Just y)
                    else void . withMPD $ play (Just (y-1))
      _ -> pure ()
