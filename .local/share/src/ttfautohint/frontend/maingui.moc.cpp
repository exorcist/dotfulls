/****************************************************************************
** Meta object code from reading C++ file 'maingui.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "maingui.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'maingui.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Main_GUI_t {
    QByteArrayData data[27];
    char stringdata0[371];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Main_GUI_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Main_GUI_t qt_meta_stringdata_Main_GUI = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Main_GUI"
QT_MOC_LITERAL(1, 9, 5), // "about"
QT_MOC_LITERAL(2, 15, 0), // ""
QT_MOC_LITERAL(3, 16, 12), // "browse_input"
QT_MOC_LITERAL(4, 29, 13), // "browse_output"
QT_MOC_LITERAL(5, 43, 14), // "browse_control"
QT_MOC_LITERAL(6, 58, 16), // "browse_reference"
QT_MOC_LITERAL(7, 75, 9), // "check_min"
QT_MOC_LITERAL(8, 85, 9), // "check_max"
QT_MOC_LITERAL(9, 95, 11), // "check_limit"
QT_MOC_LITERAL(10, 107, 12), // "check_dehint"
QT_MOC_LITERAL(11, 120, 14), // "check_no_limit"
QT_MOC_LITERAL(12, 135, 19), // "check_no_x_increase"
QT_MOC_LITERAL(13, 155, 24), // "check_default_stem_width"
QT_MOC_LITERAL(14, 180, 14), // "absolute_input"
QT_MOC_LITERAL(15, 195, 15), // "absolute_output"
QT_MOC_LITERAL(16, 211, 16), // "absolute_control"
QT_MOC_LITERAL(17, 228, 18), // "absolute_reference"
QT_MOC_LITERAL(18, 247, 19), // "set_ref_idx_box_max"
QT_MOC_LITERAL(19, 267, 16), // "check_number_set"
QT_MOC_LITERAL(20, 284, 19), // "check_family_suffix"
QT_MOC_LITERAL(21, 304, 16), // "clear_status_bar"
QT_MOC_LITERAL(22, 321, 11), // "start_timer"
QT_MOC_LITERAL(23, 333, 11), // "check_watch"
QT_MOC_LITERAL(24, 345, 11), // "watch_files"
QT_MOC_LITERAL(25, 357, 9), // "check_run"
QT_MOC_LITERAL(26, 367, 3) // "run"

    },
    "Main_GUI\0about\0\0browse_input\0browse_output\0"
    "browse_control\0browse_reference\0"
    "check_min\0check_max\0check_limit\0"
    "check_dehint\0check_no_limit\0"
    "check_no_x_increase\0check_default_stem_width\0"
    "absolute_input\0absolute_output\0"
    "absolute_control\0absolute_reference\0"
    "set_ref_idx_box_max\0check_number_set\0"
    "check_family_suffix\0clear_status_bar\0"
    "start_timer\0check_watch\0watch_files\0"
    "check_run\0run"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Main_GUI[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  139,    2, 0x08 /* Private */,
       3,    0,  140,    2, 0x08 /* Private */,
       4,    0,  141,    2, 0x08 /* Private */,
       5,    0,  142,    2, 0x08 /* Private */,
       6,    0,  143,    2, 0x08 /* Private */,
       7,    0,  144,    2, 0x08 /* Private */,
       8,    0,  145,    2, 0x08 /* Private */,
       9,    0,  146,    2, 0x08 /* Private */,
      10,    0,  147,    2, 0x08 /* Private */,
      11,    0,  148,    2, 0x08 /* Private */,
      12,    0,  149,    2, 0x08 /* Private */,
      13,    0,  150,    2, 0x08 /* Private */,
      14,    0,  151,    2, 0x08 /* Private */,
      15,    0,  152,    2, 0x08 /* Private */,
      16,    0,  153,    2, 0x08 /* Private */,
      17,    0,  154,    2, 0x08 /* Private */,
      18,    0,  155,    2, 0x08 /* Private */,
      19,    0,  156,    2, 0x08 /* Private */,
      20,    0,  157,    2, 0x08 /* Private */,
      21,    0,  158,    2, 0x08 /* Private */,
      22,    0,  159,    2, 0x08 /* Private */,
      23,    0,  160,    2, 0x08 /* Private */,
      24,    0,  161,    2, 0x08 /* Private */,
      25,    0,  162,    2, 0x08 /* Private */,
      26,    0,  163,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Main_GUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Main_GUI *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->about(); break;
        case 1: _t->browse_input(); break;
        case 2: _t->browse_output(); break;
        case 3: _t->browse_control(); break;
        case 4: _t->browse_reference(); break;
        case 5: _t->check_min(); break;
        case 6: _t->check_max(); break;
        case 7: _t->check_limit(); break;
        case 8: _t->check_dehint(); break;
        case 9: _t->check_no_limit(); break;
        case 10: _t->check_no_x_increase(); break;
        case 11: _t->check_default_stem_width(); break;
        case 12: _t->absolute_input(); break;
        case 13: _t->absolute_output(); break;
        case 14: _t->absolute_control(); break;
        case 15: _t->absolute_reference(); break;
        case 16: _t->set_ref_idx_box_max(); break;
        case 17: _t->check_number_set(); break;
        case 18: _t->check_family_suffix(); break;
        case 19: _t->clear_status_bar(); break;
        case 20: _t->start_timer(); break;
        case 21: _t->check_watch(); break;
        case 22: _t->watch_files(); break;
        case 23: _t->check_run(); break;
        case 24: _t->run(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Main_GUI::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_Main_GUI.data,
    qt_meta_data_Main_GUI,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Main_GUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Main_GUI::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Main_GUI.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int Main_GUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
